# We use HOCON to provide configurations for our datasets

project {
  name = "mdedup"
  description = "taking care of MDedup project"
}
datasets = [
  amazon_walmart
  cddb
  census
  cora
  dblp_scholar
  hotels
  ncvoters
  restaurants
]
mdedup_base_dir = "/data/projects/mdedup/"  # jokoum-laptop
#mdedup_base_dir = "/home/ioannis.koumarelas/projects/mdedup/"  # isfet

data_dir = "/data/datasets/duplicate_detection/prepared/"
hymd_base_dir =   ${mdedup_base_dir}"workspace/hymd/"
hymd_output_dir = ${hymd_base_dir}"gold/"

workspace_dir= ${mdedup_base_dir}"workspace/"
selection_dir = ${workspace_dir}"selection/"
prediction_dir= ${workspace_dir}"prediction/"
exploration_dir= ${workspace_dir}"exploration/"


database_ip = "localhost"
#database_ip = "172.16.64.11" # seschat
database_name = "mdedup"
database_name_tler = "mdedup_tler"
database_username = "root2"
database_password = "toor2"
database_driver="com.mysql.cj.jdbc.Driver"
database_connection_port="3306"
database_connection_url="jdbc:mysql://"${database_ip}":"${database_connection_port}"/"${database_name}"?testOnBorrow=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
database_connection_url_tler="jdbc:mysql://"${database_ip}":"${database_connection_port}"/"${database_name_tler}"?testOnBorrow=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"

java_vm_parameters = "-Xmx12g"
# java_vm_parameters = "-Xmx80g" # isfet

#csvsql = "/home/jokoum/anaconda3/bin/csvsql"
#csvsql = "/opt/anaconda/bin/csvsql"
#csvsql = "/home/jokoum/.local/bin/csvsql"
csvsql = "/opt/anaconda/envs/mdedup_utils/bin/csvsql"

mdedup_jar = ${mdedup_base_dir}"/code/mdedup/target/mdedup-jar-with-dependencies.jar"
hymd_jar = ${mdedup_base_dir}"code/md-demo-1.2-SNAPSHOT-jar-with-dependencies.jar"
hymd_config = ${hymd_base_dir}"config/levenshtein_0.7.config.json"

hymd {
  amazon_walmart {
    attributes = [brand, category, dataset, dimensions, imageurl, longdescr, modelno, orig_techdetails, price,
                  shipweight, shortdescr, techdetails, title]
    long_attributes = [longdescr, orig_techdetails, techdetails] # > 100 median chars
    limit = None # 24629
    hymd = "80cf22524a2e6dd23fc60bb74ecc8bfb.txt"
  }
  cddb = {
    attributes = [artist, category, genre, title, tracks, year]
    list_attributes = [tracks]
    long_attributes = [tracks]
    limit = None
    hymd = "4c8f1c55859eed1ddf6aa03f0f919b97.txt"
  }
  census = {
    attributes = [first_name, last_name, middle_name, street_address, zip_code]
    limit = None
    hymd = "f643fae7a1f0bd0a54d58a758b4a0925.txt"
  }
  cora {
    attributes = [address, authors, booktitle, date, editor, institution, journal, pages, publisher, title, type,
      volume, year]
    list_attributes = [authors]
    limit = None # 1826
    hymd = "6a4183a3f9d5ba9e65a0c84e58adfc89.txt"
  }
  dblp_scholar {
    attributes = [authors, dataset, title, venue, year]
    limit = None # 66880
    hymd = "135244f2e67bb3a59d65cc87a8492807.txt"
  }
  hotels {
    attributes = [city, country_code, hotel_name, mc_code, state_code, street_address1, zip,
      duns_number, source_type, creation_date
    ]
    limit = None # 364966
    hymd = "f55a8b27a1cfb82b4e6dd6ed3b889b24.txt"
  }
  ncvoters {
#      all_attributes = [NC_house_abbrv, NC_house_desc, NC_senate_abbrv, NC_senate_desc,
#                    age, age_group, area_cd, birth_place, cancellation_dt, confidential_ind, cong_dist_abbrv,
#                    cong_dist_desc, county_commiss_abbrv, county_commiss_desc, county_desc, county_id,
#                    dist_1_abbrv, dist_1_desc, ethnic_code, ethnic_desc,
#                    first_name, house_num, judic_dist_abbrv, judic_dist_desc, last_name, load_dt, midl_name,
#                    municipality_abbrv, municipality_desc, party_cd, party_desc, phone_num, precinct_abbrv,
#                    precinct_desc, race_code, race_desc, reason_cd, registr_dt, res_city_desc, school_dist_abbrv,
#                    school_dist_desc, sex, sex_code, state_cd, status_cd, street_name, street_type_cd,
#                    super_court_abbrv, unit_num, voter_status_desc, voter_status_reason_desc, vtd_abbrv, vtd_desc,
#                    ward_abbrv, ward_desc, zip_code, "\ufeffsnapshot_dt"]
    attributes = [
      age, area_cd, birth_place, cancellation_dt, county_desc, county_id, ethnic_desc, first_name,
      house_num, last_name, midl_name, party_desc, phone_num, race_desc, reason_cd, registr_dt, res_city_desc, sex,
      state_cd, status_cd, street_name, street_type_cd, voter_status_desc, voter_status_reason_desc, zip_code]
  # > 40% completeness AND remove "code" attributes: race_code, party_cd, ethnic_code, sex_code, also age_group AND
  #    remove "ncid", "voter_reg_num" AND low uniqueness: confidential_ind, load_dt, snapshot_dt
    limit = None
    hymd = "1330ba4f0c1c23e24aa75c34a85a18aa.txt"
  }
  restaurants {
    attributes = [address, city, name, phone, type]
    limit = None
    hymd = "5309703ae56ae5719d9a53fe777e55ae.txt"
  }

}
selection {
  configuration {
    java_vm_parameters = ${java_vm_parameters}
    topk = 20
    max_level = 5
  }
  f1_by_maxlevel_data = ${selection_dir}"experiment_maxlevel.tsv"
  grid_search_parameters_experiment = ${selection_dir}"grid_search_parameters.pdf"
}
exploration {
  configuration {
    java_vm_parameters = ${java_vm_parameters}
  }
  relation = "mdedup_exploration"
}
prediction {configuration {
    java_vm_parameters = ${java_vm_parameters}
  }
  relation = "mdedup_prediction"
}
boosting_predict {
  configuration {
      java_vm_parameters = ${java_vm_parameters}
      classification_algorithms = [RANDOM_FOREST, SUPPORT_VECTOR_MACHINES, THRESHOLD, LOGISTIC_REGRESSION, NAIVE_BAYES, K_NEAREST_NEIGHBORS]
  }
  relation = "mdedup_boosting"
}

schemata {
  mdedup_database = """CREATE DATABASE `mdedup` /*!40100 DEFAULT CHARACTER SET utf8 */;"""
  # Dataset and pair similarities relations will be generated by code. (SQLAlchemy)
  # Additionally, any other table not defined here, will be also auto-generated by code. Such as:
  # mdedup_hymd (results of HyMD), mdedup_exploration (results of Feature Engineering phase), mdedup_prediction
  #  (results of prediction phase - testing).
}

mdc_features = [cardinality, completeness, confidence, conviction, lift, set_size_ATTRIBUTE_INTERSECTION,
  set_size_ATTRIBUTE_JACCARD, set_size_ATTRIBUTE_MAX, set_size_ATTRIBUTE_MEDIAN, set_size_ATTRIBUTE_MIN,
  set_size_ATTRIBUTE_STDEV, set_size_ATTRIBUTE_UNION, set_size_COLUMN_MATCH_INTERSECTION, set_size_COLUMN_MATCH_JACCARD,
  set_size_COLUMN_MATCH_MAX, set_size_COLUMN_MATCH_MEDIAN, set_size_COLUMN_MATCH_MIN, set_size_COLUMN_MATCH_STDEV,
  set_size_COLUMN_MATCH_UNION, set_size_SIMILARITY_MEASURE_INTERSECTION, set_size_SIMILARITY_MEASURE_JACCARD,
  set_size_SIMILARITY_MEASURE_MAX, set_size_SIMILARITY_MEASURE_MEDIAN, set_size_SIMILARITY_MEASURE_MIN,
  set_size_SIMILARITY_MEASURE_STDEV, set_size_SIMILARITY_MEASURE_UNION, set_size_THRESHOLD_INTERSECTION,
  set_size_THRESHOLD_JACCARD, set_size_THRESHOLD_MAX, set_size_THRESHOLD_MEDIAN, set_size_THRESHOLD_MIN,
  set_size_THRESHOLD_STDEV, set_size_THRESHOLD_UNION, support, thresholds_max, thresholds_median, thresholds_min,
  uniqueness
]