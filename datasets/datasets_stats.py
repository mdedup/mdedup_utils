import pandas as pd
import sqlalchemy

from mdedup import CONF


def calculate_statistics():
    stats = []
    db_connection = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'
                                      .format(CONF["database_username"],
                                              CONF["database_password"],
                                              CONF["database_ip"],
                                              CONF["database_name"]))

    for i, dataset in enumerate(CONF["datasets"]):
        df_data = pd.read_sql("SELECT * FROM " + dataset, con=db_connection, coerce_float=False)
        df_data = df_data.astype(str)
        df_data.set_index(keys=["id"], inplace=True, drop=False)
        df_data.index = df_data.index.map(str)

        columns = sorted(list(df_data.columns))

        dataset_stats = []
        for c in columns:
        # for c in ["street_address3"]:
            completeness = (dataset, c, "completeness", calculate_completeness(df_data[c]))
            dataset_stats.append(completeness)
            print(completeness)

            median_length = (dataset, c, "median_length", calculate_median_length(df_data[c]))
            dataset_stats.append(median_length)
            print(median_length)

            uniqueness = (dataset, c, "uniqueness", calculate_uniqueness(df_data[c]))
            dataset_stats.append(uniqueness)
            print(uniqueness)

        stats.extend(dataset_stats)

        df = pd.DataFrame().from_records(dataset_stats, columns=["dataset", "attribute", "metric", "metric_value"])
        df.to_sql(con=db_connection, name='dataset_stats', if_exists="append", index=False, chunksize=10000)


def calculate_completeness(l: list):
    counter = 0
    for i in range(len(l)):
        v = l[i]
        if v is None or v == pd.np.nan:
            counter += 1
        else:
            v = v.strip()
            if v == "None" or v == "":
                counter += 1
    return 1.0 - (float(counter) / len(l))


def calculate_median_length(l: list):
    lengths = []
    for i in range(len(l)):
        v = l[i]
        if v is None or v == pd.np.nan:
            lengths.append(0)
        else:
            v = v.strip()
            if v == "None" or v == "":
                lengths.append(0)
            else:
                lengths.append(len(v))
    lengths.sort()
    return lengths[int(len(lengths) / 2)]


def calculate_uniqueness(l: list):
    values = set()
    for i in range(len(l)):
        v = l[i]
        if v is None or v == pd.np.nan:
            v = ""
        else:
            v = v.strip()
        values.add(v)

    if "" in values:
        values.remove("")

    return len(values) / float(len(l))


if __name__ == '__main__':
    calculate_statistics()