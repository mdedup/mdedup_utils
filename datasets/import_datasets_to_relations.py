import os

import pandas as pd
import sqlalchemy

from mdedup import CONF


class DatasetsImporter:
    def convert_datasets_to_relations(self):
        """
        Be sure to set the DB's default collation to UTF-8. For MySQL this is :"utf8 - default collation".
        :return:
        """
        for dataset in CONF["datasets"]:
            if dataset == "amazon_walmart":
                # Importing this dataset with csvsql is not recommended. Prefer another tool. For MySQL use the
                # importing tool from MySQL Workbench.
                continue
            print("Importing dataset: " + dataset)
            cmd = CONF["csvsql"]
            cmd += " -v"
            cmd += " --quoting 3"
            cmd += " --db mysql+pymysql://"+CONF["database_username"]+":"+CONF["database_password"]+"@"+CONF["database_ip"]+":"\
                   +CONF["database_connection_port"]+"/"+CONF["database_name"]+"?charset=utf8"
            cmd += " --tables " + dataset
            cmd += " --no-doublequote"
            cmd += " --tabs"
            cmd += " --encoding utf8"
            cmd += " --insert"
            cmd += " --overwrite"
            cmd += " --no-inference"  # do not identify as double, integer etc.
            cmd += " --snifflimit 1000000"
            cmd += " " + CONF["data_dir"] + dataset + ".tsv"
            print(cmd)
            os.system(cmd)


    def convert_gold_standards_to_single_relation(self):
        attributes = ["dataset", "id1", "id2", "class", "total_similarity"]
        for i, dataset in enumerate(CONF["datasets"]):
            print("Importing gold standard for :" + dataset)

            dpl = pd.read_csv(CONF["data_dir"] + dataset + "_DPL.tsv", sep='\t', header=0, na_filter=False, dtype=str)
            dpl["class"] = "dpl"
            dpl["dataset"] = dataset

            ndpl = pd.read_csv(CONF["data_dir"] + dataset + "_NDPL.tsv", sep='\t', header=0, na_filter=False, dtype=str)
            ndpl["class"] = "ndpl"
            ndpl["dataset"] = dataset

            ndpl.rename(index=str, columns={"participation": "total_similarity"}, inplace=True)

            pairs = pd.DataFrame(columns=attributes)
            pairs = pairs.append(dpl, ignore_index=True)
            pairs = pairs.append(ndpl, ignore_index=True)

            db_con = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'
                                              .format(CONF["database_username"], CONF["database_password"],
                                                      CONF["database_ip"], CONF["database_name"]))
            # pairs.to_sql(con=db_con, name='pairs', if_exists='replace' if i == 0 else "append", index=False, chunksize=10000)
            pairs.to_sql(con=db_con, name='pairs', if_exists="append", index=False, chunksize=10000)


def prepare_command(csvsqlpath, table_name, fpath):
    cmd = csvsqlpath
    cmd += " -v"
    cmd += " --quoting 3"
    cmd += " --db mysql://root:toor@localhost:3306/mdedup?charset=utf8"
    cmd += " --tables " + table_name
    cmd += " --no-doublequote"
    cmd += " --tabs"
    cmd += " --encoding utf8"
    cmd += " --insert"
    cmd += " --overwrite"
    cmd += " --no-inference"  # do not identify as double, integer etc.
    cmd += " --snifflimit 1000000"
    cmd += " " + fpath
    return cmd


if __name__ == '__main__':
    di = DatasetsImporter()
    di.convert_datasets_to_relations()
    di.convert_gold_standards_to_single_relation()
    # cmd = prepare_command("/opt/anaconda/bin/csvsql", "mdedup_exploration_test", "/data/tmp/mdedup_exploration_1.csv",)
    # print(cmd)