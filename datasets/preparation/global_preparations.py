import csv
import re

import pandas as pd
from tqdm import tqdm

from mdedup import CONF

pattern = re.compile('[\W_]+', re.UNICODE)

def prepare():
    for dataset in CONF["datasets"]:
        df_data = pd.read_csv(CONF["data_dir"] + dataset + ".tsv", sep="\t", quotechar='\'',
                              index_col=None, na_filter=False,
                              dtype=str, encoding="utf-8", error_bad_lines=False).fillna('')
        df_data = df_data.loc[:, ~df_data.columns.str.contains('^Unnamed')]
        data = df_data.to_dict(orient="index")

        header = sorted(list(set(df_data.columns.values)))
        with open(CONF["data_dir"] + dataset + "_prepared.tsv", 'w', newline='\n', encoding="utf-8") as fout:
            dictout = csv.DictWriter(fout, fieldnames=header, delimiter='\t')
            dictout.writeheader()
            for i, r in tqdm(data.items(), desc=dataset + " iteration", total=len(data)):
                for attr in df_data.columns.values:
                    v = r[attr]

                    # Preparations
                    v = remove_special_characters(v)
                    v = change_casing(v, "lower")

                    r[attr] = v
                dictout.writerow(r)


def remove_special_characters(v1):
    v2 = pattern.sub(" ", v1).strip()
    v2 = " ".join(v2.split(" "))
    return v2


def change_casing(v1, case:str="lower"):
    if case == "lower":
        return str(v1).lower()


if __name__ == '__main__':
    prepare()