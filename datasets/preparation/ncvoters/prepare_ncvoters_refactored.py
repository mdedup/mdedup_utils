import csv
import sys

import tqdm

csv.field_size_limit(sys.maxsize)

# target_records = 100000
# target_records = 141838  # 1%
target_records = 14183  # 0.01%
gold_standard_percentage = 1.0
gld_recs_target = int(gold_standard_percentage * target_records)

base = "/data/datasets/duplicate_detection/NCVoters/snapshots/"

data1 = base + "VR_Snapshot_20181106.tsv"
data2 = base + "VR_Snapshot_20181106_utf8.tsv"

attributes = ["ncid", "voter_reg_num"]
data3 = {attr: data2 + "_histograms_" + attr + ".tsv" for attr in attributes}

attr_to_gb = "ncid"
data4_ids_grouped_by_attr = data2 + "_grp_by_"+attr_to_gb+".tsv"

signature = str(target_records) + "_" + str(gold_standard_percentage)

data5_gs_pairs = data2 + "_" + signature + "_gold_standard_pairs.tsv"
data5_gs_ids = data2 + "_" + signature + "_gold_standard_ids.tsv"
data5_recs = data2 + "_" + signature + "_records_to_insert.tsv"
data5_ids = data2 + "_" + signature + "_ids_to_insert.tsv"

def convert_to_utf8():
    # STEP 1: Convert to utf-8
    with open(data1, 'rt', encoding="utf-16-le") as fin, open(data2, 'wt', encoding="utf-8") as fout:
        i = 0
        for row in tqdm.tqdm(fin, desc="Converting UTF-16-le to UTF-8"):
            # row = row.decode('utf-16').encode('utf-8') # row = str(row)
            row = row.replace('\x00', '')
            row = (" ".join(row.split(" ")))

            fout.write(("id" if (i == 0) else str(i)) + "\t")
            fout.write(row)

            i += 1


def generate_frequency_histogram():
    id_count = {}

    # STEP 2.1: Generate frequencies. Since DictReader skips some lines (empty? with some errors?), we read them manually.
    for attr in attributes:
        with open(data2, 'rt', encoding='utf-8') as fin:
            i = 0
            header = {}
            for row in tqdm.tqdm(fin, desc="Generating frequency histogram"):
                i += 1
                row = row.replace("\n", "")
                if i == 1:
                    header = {v: i for i, v in enumerate(row.split("\t"))}
                    print(header)
                    continue
                toks = row.split("\t")
                v = toks[header[attr]].strip()
                id_count[v] = 1 if v not in id_count else id_count[v] + 1
                # print(str(i))
                # if i == 100000:
                #	break

        id_count_list = [(k, v) for k, v in id_count.items()]
        id_count_list_sorted = sorted(id_count_list, key=lambda x: x[1], reverse=True)

        # with open(in_file + "_statistics_"+ attr +".tsv", "wt", encoding='"ISO-8859-1"') as fout:
        with open(data3[attr], "wt", encoding="utf-8") as fout:
            csvout = csv.DictWriter(fout, delimiter="\t", fieldnames=["id", "count"])
            csvout.writeheader()
            for i in id_count_list_sorted:
                csvout.writerow({"id": i[0], "count": i[1]})


def generate_gold_standard_dataset():
    attr_eq1_set, attr_gt1_set = import_eq1_gt1(attr="ncid")
    attr_eq1_lst, attr_gt1_lst = list(attr_eq1_set), list(attr_gt1_set)

    grp_attr_all_ids = _group_ids_by_attr_id(attr_gt1_set, "write", attr="ncid")
    grp_attr_all_ids = _group_ids_by_attr_id(attr_gt1_set, "read", attr="ncid")
    # exit(0)

    # idxs_gt1 = [i for i in range(len(ncid_gt1_lst))]
    # idxs_gt1 = [i for i in range(len(attr_gt1_set))]
    idxs_eq1 = None
    # shuffle(idxs_gt1)
    # if gold_standard_percentage < 1.0:
    #     # idxs_eq1 = [i for i in range(len(ncid_eq1_lst))]
    #     idxs_eq1 = [i for i in range(len(attr_eq1_set))]
    #     shuffle(idxs_eq1)

    # Write pairs
    gld_recs_inserted_id = export_gs_pairs(attr_gt1_lst, grp_attr_all_ids)

    # with open(data5_gs_ids, 'wt', encoding='utf-8') as fout_ids:
    #     csvout_id = csv.DictWriter(fout_ids, delimiter="\t", fieldnames=["id"])
    #     csvout_id.writeheader()
    #     for x in gld_recs_inserted_id:
    #         csvout_id.writerow({"id": x})

    export_pairs_records_ids(attr_eq1_lst, attr_gt1_set, gld_recs_inserted_id, idxs_eq1)


def export_pairs_records_ids(attr_eq1_lst, attr_gt1_set, gld_recs_inserted_id, idxs_eq1, attr="ncid"):
    print("Writing gold standard pairs, gold standard records and gold standard ids")
    with open(data2, 'rt', encoding='utf-8') as fin, \
            open(data5_recs, 'wt', encoding="utf-8") as fout_rec, \
            open(data5_ids, 'wt', encoding="utf-8") as fout_id:
        i_gt1 = 0
        i_eq1 = 0

        non_gld_recs = target_records - len(gld_recs_inserted_id)

        if gold_standard_percentage < 1.0:
            recs_eq1_set = set([attr_eq1_lst[x] for x in idxs_eq1[0:non_gld_recs * 20]])

        csvin = csv.DictReader(fin, delimiter='\t', quotechar='"')
        csvout_rec = csv.DictWriter(fout_rec, delimiter="\t", fieldnames=csvin.fieldnames)
        csvout_rec.writeheader()
        fout_id.write("id" + "\n")

        i = 0

        for row in csvin:
            i += 1
            print(str(i))

            if row["id"] in gld_recs_inserted_id:
                csvout_rec.writerow(row)
                fout_id.write(row["id"] + "\n")

                i_gt1 += 1
                print("i_gt1: " + str(i_gt1))
            elif row[attr] in attr_gt1_set:
                print("continuing")
                continue
            else:
                if gold_standard_percentage < 1.0:
                    if i_eq1 < non_gld_recs and row[attr] in recs_eq1_set:
                        csvout_rec.writerow(row)
                        fout_id.write(row["id"] + "\n")
                        i_eq1 += 1
                        print("i_eq1: " + str(i_eq1))

            if i_gt1 == len(gld_recs_inserted_id) and i_eq1 == non_gld_recs:
                break


def export_gs_pairs(attr_gt1_lst, grp_attr_all_ids):
    print("Writing pairs")
    gld_recs_inserted_id = set()
    with open(data5_gs_pairs, 'wt', encoding='utf-8') as fout_pairs:
        csvout_pairs = csv.DictWriter(fout_pairs, delimiter="\t", fieldnames=["id1", "id2"])
        csvout_pairs.writeheader()
        for attr_id in grp_attr_all_ids:
            if len(gld_recs_inserted_id) >= gld_recs_target:
                break
            group = grp_attr_all_ids[attr_id]
            print(group)
            # gld_recs_inserted += len(group)
            for i in range(0, len(group)):
                for j in range(i + 1, len(group)):
                    csvout_pairs.writerow({"id1": group[i], "id2": group[j]})

            for x in group:
                gld_recs_inserted_id.add(x)

            # print(str(idx) + "\t" + str(len(gld_recs_inserted_id)))

    return gld_recs_inserted_id

def import_eq1_gt1(attr="ncid"):

    attr_gt1_set = set()
    attr_eq1_count = 0
    # ncid_eq1_lst = []
    ncid_eq1_set = set()
    with open(data3[attr], "rt", encoding="utf-8") as fin:
        csvin = csv.DictReader(fin, delimiter='\t', quotechar='"')
        i = 0
        for row in tqdm.tqdm(csvin, desc="Reading "+attr+" histograms"):
            if int(row["count"]) > 1:
                attr_gt1_set.add(row["id"])
            else:
                if gold_standard_percentage < 1.0:
                    attr_eq1_count += 1
                    # ncid_eq1_lst.append(row["id"])
                    ncid_eq1_set.add(row["id"])
                else:
                    break
    return ncid_eq1_set, attr_gt1_set


def _group_ids_by_attr_id(ncid_gt1_set, mode, attr="ncid"):
    # Group by ncid, all the hpi_ids
    grp_attr_all_ids = {}
    if mode == "write":
        with open(data2, 'rt', encoding="utf-8") as fin:
            csvin = csv.DictReader(fin, delimiter='\t', quotechar='"')
            for row in tqdm.tqdm(csvin, desc="Reading " + data2):
                if row[attr] in ncid_gt1_set:
                    if row[attr] not in grp_attr_all_ids:
                        grp_attr_all_ids[row[attr]] = set()
                    grp_attr_all_ids[row[attr]].add(row["id"])
        print("To pairs")
        grp_ncid_all_hpi_ids_pairs = [(k, v) for k, v in grp_attr_all_ids.items()]
        print("Sort pairs")
        grp_ncid_all_ids_pairs_sorted = sorted(grp_ncid_all_hpi_ids_pairs, key=lambda x: len(x[1]), reverse=True)

        print("Write pairs")
        with open(data4_ids_grouped_by_attr, 'wt', encoding="utf-8") as fout:
            fout.write(attr + "\t" + "group_ids" + "\n")
            for pair in grp_ncid_all_ids_pairs_sorted:
                k, v = pair[0], pair[1]
                fout.write(str(k) + "\t" + ",".join(v) + "\n")
    elif mode == "read":
        with open(data4_ids_grouped_by_attr, 'rt', encoding="utf-8") as fin:
            csvin = csv.DictReader(fin, delimiter='\t', quotechar='"')
            for row in csvin:
                k, v = row[attr], row["group_ids"]
                grp_attr_all_ids[k] = v.split(",")
        return grp_attr_all_ids




def main():
    # convert_to_utf8()
    # generate_frequency_histogram()
    generate_gold_standard_dataset()

if __name__ == '__main__':
    main()