import csv
import os

import pandas as pd
from tqdm import tqdm


def generate_metadata_attributes_CSV_incrementally(data_file, metadata_dir):
    if not os.path.isdir(metadata_dir):
        os.makedirs(metadata_dir)

    n = 0
    with open(data_file, "rt",  encoding='utf-8') as fin:
        csvin = csv.DictReader(fin, delimiter="\t", quoting=csv.QUOTE_NONE)
        attributes = csvin.fieldnames
    with open(data_file, "rt", encoding='utf-8') as fin:
        for r in fin:
            n += 1

    print("Attributes" + str(attributes) + " from " + str(n) + " records.")
    attr_to_metrics = {}

    for attribute in attributes:
        metrics, frequencies_list = calculate_metrics_for_attribute(data_file, attribute, n)
        attr_to_metrics[attribute] = metrics

    attribute_metrics_list = []
    metrics = ["uniqueness", "completeness", "counter_total", "counter_valid", "counter_invalid"]
    for attribute in attr_to_metrics:
        attribute_metrics = [attribute]
        for metric in metrics:
            attribute_metrics.append(attr_to_metrics[attribute][metric])
        attribute_metrics_list.append(tuple(attribute_metrics))

    df = pd.DataFrame.from_records(data=attribute_metrics_list, columns=["attribute"] + metrics)
    df.index.name = 'attribute'
    return df


def calculate_metrics_for_attribute(data_file, attribute, n):
    frequencies = {}

    counter_valid = 0
    counter_invalid = 0
    with open(data_file, "rt",  encoding='utf-8') as fin:
        csvin = csv.DictReader(fin, delimiter="\t", quoting=csv.QUOTE_NONE)
        for r in tqdm(csvin, desc="Attribute: " + attribute + " ", total=n):
            v = r[attribute].strip()
            if v not in frequencies:
                frequencies[v] = 0
            frequencies[v] += 1

            if v != "":
                counter_valid += 1
            else:
                counter_invalid += 1
    if counter_valid > 0:
        uniqueness = float(len(frequencies)) / counter_valid
        completeness = float(counter_valid) / (counter_valid + counter_invalid)
        counter_total = counter_valid + counter_invalid
    else:
        uniqueness, completeness, counter_total = 0, 0, 0

    metrics = {
        "uniqueness": uniqueness,
        "completeness": completeness,
        "counter_total": counter_total,
        "counter_valid": counter_valid,
        "counter_invalid": counter_invalid
    }

    frequencies_list = [(k, v) for k, v in frequencies.items()]

    return metrics, frequencies_list


def main():
    file = "/data/projects/data_timeline/data/hotels/prepared/merged_1000.tsv"
    parent_dir = os.path.abspath(os.path.join(file, os.pardir))
    filename = os.path.basename(file)
    basename, extension = os.path.splitext(filename)
    extension = extension[1:]

    metadata_dir = parent_dir + "/" + "statistics" + "/" + basename + "/"

    if not os.path.exists(metadata_dir):
        os.makedirs(metadata_dir)

    group_by_attribute = None

    if extension.lower() in ["tsv", "csv"]:
        generate_metadata_attributes_CSV_incrementally(file, metadata_dir, group_by_attribute)
    elif extension.lower() in ["sqlite"]:
        pass
        # generate_metadata_attributes_SQL(file, metadata_dir, group_by_attribute)

if __name__ == '__main__':
    main()