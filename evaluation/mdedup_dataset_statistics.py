import numbers

import pandas as pd
import sqlalchemy
from sqlalchemy.util import OrderedDict

from mdedup import CONF

db_connection = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'
                                             .format(CONF["database_username"],
                                                     CONF["database_password"],
                                                     CONF["database_ip"],
                                                     CONF["database_name"]
                                                     # "mdedup_seschat"
                                                     )
                                         )

dataset_prettify = {
        "amazon_walmart": "Amazon-Walmart",
        "cddb": "CDDB",
        "census": "Census",
        "cora": "Cora",
        "dblp_scholar": "DBLP-Scholar",
        "hotels": "Hotels",
        "ncvoters": "NCVoters",
        "restaurants": "Restaurants"
    }

def gather_data_for_datasets():
    h = None
    info = OrderedDict()
    # for dataset in sorted(CONF["datasets"]):
    for dataset in ["census", "restaurants", "dblp_scholar", "cddb", "hotels", "cora", "amazon_walmart", "ncvoters"]:
        info[dataset] = gather_data_per_dataset(dataset)
        print(info[dataset])
        if h == None:
            h = list(info[dataset])
    df = pd.DataFrame.from_dict(info).transpose()
    df = df[h]  # Re-order columns
    df.insert(0, "dataset", df.index)

    df = df.replace({"dataset": dataset_prettify})

    def move_element(odict, thekey, newpos):
        odict[thekey] = odict.pop(thekey)
        i = 0
        for key, value in odict.items():
            if key != thekey and i >= newpos:
                odict[key] = odict.pop(key)
            i += 1
        return odict

    def represents_int(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    def represents_float(s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    def print_number(s):
        if isinstance(s, int) or isinstance(s, numbers.Integral):
            return '{:,}'.format(s)
        elif isinstance(s, float):
            return "{0:,.2f}".format(s)
        else:
            return s

    for key in df.columns.values:
        if key in ["Records", "Attributes", "DPL", "NDPL", "MDs", "MDCs\_selection", "MDCs\_predicted"]:
            df[key] = df[key].apply(lambda x: '{:,}'.format(int(x)))

    print(df.to_latex(index=False))

def gather_data_per_dataset(dataset):
    info = OrderedDict()
    info["Records"] = get_number_of_records(dataset)
    info["Attributes"] = len(CONF["hymd"][dataset]["attributes"])
    info["DPL"] = get_number_of_pairs(dataset, class_of = "dpl")
    info["NDPL"] = get_number_of_pairs(dataset, class_of = "ndpl")
    info["MDs"] = get_number_of_mds(dataset)
    # info["MDCs_possible"] = pow(2, info["MDs"])
    info["MDCs_selection"] = get_number_of_mdcs(dataset, "selection")
    # info["MDCs_max_size_selection"] = get_mdcs_max_size(dataset, "selection")
    info["MDCs_predicted"] = get_number_of_mdcs(dataset, "prediction")

    info["ExecutionTime_MDDis"] = format_seconds(get_hymd_execution_time(dataset))
    info["ExecutionTime_Selection"] = format_seconds(get_mdcs_execution_time(dataset, "selection"))
    info["ExecutionTime_Prediction"] = format_seconds(get_mdcs_execution_time(dataset, "prediction"))
    info["ExecutionTime_Classification"] = format_seconds(get_competitors_execution_time(dataset, "trained_on_mdedup_prediction"))
    return info

def get_number_of_records(dataset):
    return int(_get_value_from_query("select count(*) as v from " + dataset))

def get_number_of_pairs(dataset, class_of):
    return int(_get_value_from_query("select count(*) as v from pairs where dataset='" + dataset+"' and `class`='" + class_of + "'"))

def get_number_of_mds(dataset):
    return int(_get_value_from_query("select count(*) as v from mdedup_hymd where dataset='" + dataset + "'"))

def _filter_mdcs():
    sqlstr = " and enabled_expansion_phase = 1 and (phase=\"selection\" or (phase=\"prediction\" and features_in_regression <> \"complete_feature_set\")) "
    return sqlstr

def get_number_of_mdcs(dataset, phase):
    return int(_get_value_from_query("select count(distinct(ids)) as v from mdedup_mdcs where dataset='" + dataset + "' and `phase`='" + phase + "'" + _filter_mdcs()))

def get_mdcs_max_size(dataset, phase):
    return _get_value_from_query("select max(level) as v from mdedup_mdcs where dataset='" + dataset + "' and `phase`='" + phase + "'" + _filter_mdcs())

def get_mdcs_execution_time(dataset, phase):
    t = _get_value_from_query("select max(total_execution_duration) as v from mdedup_mdcs where dataset='" + dataset + "' and `phase`='" + phase + "'" + _filter_mdcs())
    new_t = round(float(t) / 1000.0) if t not in ("", None) else -1.0
    return new_t

def get_competitors_execution_time(dataset, description):
    t = _get_value_from_query("select max(classification_total_duration) as v from mdedup_boosting where dataset='" + dataset + "' and `description`='" + description + "'")
    new_t = round(float(t) / 1000.0) if t not in ("", None) else -1.0
    return new_t

def get_hymd_execution_time(dataset):
    fname = CONF["hymd"][dataset]["hymd"].split(".")[0]
    dir = CONF["hymd_base_dir"] + "report/" + fname + "/"

    files = ["de.hpi.is.md.hybrid.HybridExecutor.discover.csv",
             "de.hpi.is.md.hybrid.impl.preprocessed.PreprocessorImpl.get.csv"]

    time = 0.0
    for f in files:
        df = pd.read_csv(dir + f)
        times = list(df["max"])
        for t in times:
            time += t

    t_in_sec = float(time / 1000.0)
    return t_in_sec

def _get_value_from_query(q):
    try:
        df = pd.read_sql(q, con=db_connection, coerce_float=False).iloc[0]
        v = df["v"]
        if v is None:
            return ""
        else:
            return v
    except:
        return ""

def format_seconds(s):
        hours = s // 3600
        s %= 3600
        minutes = s // 60
        s %= 60
        seconds = s

        # return days, hours, minutes, seconds
        formattedstr = '{:02}:{:02}:{:02}'.format(int(hours), int(minutes), int(seconds))
        return formattedstr

if __name__ == '__main__':
    gather_data_for_datasets()
