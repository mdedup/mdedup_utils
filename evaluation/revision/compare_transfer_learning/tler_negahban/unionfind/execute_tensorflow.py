import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn import preprocessing
from sklearn.metrics import precision_recall_fscore_support
from tqdm import tqdm


# def execute():
#     np.random.seed(101)
#     tf.set_random_seed(101)
#
#
#     # Genrating random linear data
#     # There will be 50 data points ranging from 0 to 50
#     x = np.linspace(0, 50, 50)
#     y = np.linspace(0, 50, 50)
#
#     # Adding noise to the random linear data
#     x += np.random.uniform(-4, 4, 50)
#     y += np.random.uniform(-4, 4, 50)
#
#     n = len(x) # Number of data points
#
#
#     # Plot of Training Data
#     plt.scatter(x, y)
#     plt.xlabel('x')
#     plt.xlabel('y')
#     plt.title("Training Data")
#     plt.show()
#
#     X = tf.placeholder("float")
#     Y = tf.placeholder("float")
#
#     W = tf.Variable(np.random.randn(), name = "W")
#     b = tf.Variable(np.random.randn(), name = "b")
#
#
#     learning_rate = 0.01
#     training_epochs = 1000
#
#
#     # Hypothesis
#     y_pred = tf.add(tf.multiply(X, W), b)
#
#     # Mean Squared Error Cost Function
#     cost = tf.reduce_sum(tf.pow(y_pred-Y, 2)) / (2 * n)
#
#     # Gradient Descent Optimizer
#     optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
#
#     # Global Variables Initializer
#     init = tf.global_variables_initializer()
#
#     # Starting the Tensorflow Session
#     with tf.Session() as sess:
#         # Initializing the Variables
#         sess.run(init)
#
#         # Iterating through all the epochs
#         for epoch in range(training_epochs):
#
#             # Feeding each data point into the optimizer using Feed Dictionary
#             for (_x, _y) in zip(x, y):
#                 sess.run(optimizer, feed_dict={X: _x, Y: _y})
#
#             # Displaying the result after every 50 epochs
#             if (epoch + 1) % 50 == 0:
#                 # Calculating the cost a every epoch
#                 c = sess.run(cost, feed_dict={X: x, Y: y})
#                 print("Epoch", (epoch + 1), ": cost =", c, "W =", sess.run(W), "b =", sess.run(b))
#
#             # Storing necessary values to be used outside the Session
#         training_cost = sess.run(cost, feed_dict={X: x, Y: y})
#         weight = sess.run(W)
#         bias = sess.run(b)
#
#     # Calculating the predictions
#     predictions = weight * x + bias
#     print("Training cost =", training_cost, "Weight =", weight, "bias =", bias, '\n')
#
#
#     # Plotting the Results
#     plt.plot(x, y, 'ro', label ='Original data')
#     plt.plot(x, predictions, label ='Fitted line')
#     plt.title('Linear Regression Result')
#     plt.legend()
#     plt.show()


# def execute_tler_tensorflow_working():
#     workspace_path = "/data/projects/mdedup/workspace/testing_tensorflow/"
#     # df_X_train, df_Y_train, df_X_test, df_Y_test, np_X_train, np_Y_train, np_X_test, np_Y_test = execute_tler()
#     # df_X_train.to_pickle(workspace_path + "X_train.pkl")
#     # df_Y_train.to_pickle(workspace_path + "Y_train.pkl")
#     # df_X_test.to_pickle(workspace_path + "X_test.pkl")
#     # df_Y_test.to_pickle(workspace_path + "Y_test.pkl")
#
#     df_X_train = pd.read_pickle(workspace_path + "X_train.pkl")
#     df_Y_train = pd.read_pickle(workspace_path + "Y_train.pkl")
#     df_X_test = pd.read_pickle(workspace_path + "X_test.pkl")
#     df_Y_test = pd.read_pickle(workspace_path + "Y_test.pkl")
#
#     # We will need to train for specific sources.
#     i_idx_train = (df_Y_train["part1"] == 0) | (df_Y_train["part2"] == 0)
#     i_train = df_Y_train[i_idx_train]
#     i_train.reset_index(inplace=True, drop=True)
#     i_idx_test = (df_Y_test["part1"] == 0) | (df_Y_test["part2"] == 0)
#
#     # j_idx = (df_Y_train["part1"] == 1) | (df_Y_train["part2"] == 1)
#     # j_train = df_Y_train[j_idx]
#     # j_train.reset_index(inplace=True, drop=True)
#
#
#     np_X_train = df_X_train[i_idx_train].to_numpy()
#     np_Y_train = df_Y_train[i_idx_train]["class"].to_numpy()
#     np_X_test = df_X_test[i_idx_test].to_numpy()
#     np_Y_test = df_Y_test[i_idx_test]["class"].to_numpy()
#
#     np.random.seed(101)
#     tf.set_random_seed(101)
#
#     # Genrating random linear data
#     # There will be 50 data points ranging from 0 to 50
#     # x = np.linspace(0, 50, 50)
#     # y = np.linspace(0, 50, 50)
#     #
#     # # Adding noise to the random linear data
#     # x += np.random.uniform(-4, 4, 50)
#     # y += np.random.uniform(-4, 4, 50)
#     #
#     # n = len(x)  # Number of data points
#     #
#     # # Plot of Training Data
#     # plt.scatter(x, y)
#     # plt.xlabel('x')
#     # plt.xlabel('y')
#     # plt.title("Training Data")
#     # plt.show()
#
#     n = len(np_X_train)
#     m = len(df_X_train.columns)
#
#     # Actual data
#     x = np_X_train
#     y = np_Y_train.T
#
#     X = tf.placeholder("float", shape=(m,None), name="X")
#     Y = tf.placeholder("float", shape=(1,None), name="Y")
#
#     W = tf.Variable(np.random.randn(), name="W")
#     b = tf.Variable(np.random.randn(), name="b")
#
#     learning_rate = 0.01
#     training_epochs = 100
#
#     # Hypothesis
#     y_pred = tf.add(tf.multiply(X, W), b)
#
#     print(y_pred)
#
#     # Mean Squared Error Cost Function
#     cost = tf.reduce_sum(tf.pow(y_pred - Y, 2)) / (2 * n)
#
#     # Gradient Descent Optimizer
#     optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
#
#     # Global Variables Initializer
#     init = tf.global_variables_initializer()
#
#     # Starting the Tensorflow Session
#     with tf.Session() as sess:
#         # Initializing the Variables
#         sess.run(init)
#
#         # Iterating through all the epochs
#         for epoch in range(training_epochs):
#
#             # Feeding each data point into the optimizer using Feed Dictionary
#             for (_x, _y) in zip(x, y):
#                 _x_reshaped = np.array([_x]).T
#                 # _x_reshaped = _x
#                 _y_np = np.array([_y]).T.reshape(1,1)
#                 # print("_y: " + str(_y.shape))
#                 # print("_y_np: " + str(_y_np.shape))
#                 sess.run(optimizer, feed_dict={X: _x_reshaped, Y: _y_np})
#
#             # Displaying the result after every n epochs
#             if (epoch + 1) % 10 == 0:
#                 y_reshaped = y.reshape(1,n)
#                 # Calculating the cost a every epoch
#                 c = sess.run(cost, feed_dict={X: x.transpose(), Y: y_reshaped})
#                 print("Epoch", (epoch + 1), ": cost =", c, "W =", sess.run(W), "b =", sess.run(b))
#
#             # Storing necessary values to be used outside the Session
#         x_reshaped = x.T
#         y_reshaped = np.array([y])
#         training_cost = sess.run(cost, feed_dict={X: x_reshaped, Y: y_reshaped})
#         weight = sess.run(W)
#         bias = sess.run(b)
#
#     # Calculating the predictions
#     predictions = weight * x + bias
#     print("Training cost =", training_cost, "Weight =", weight, "bias =", bias, '\n')
#
#     # # Plotting the Results
#     # plt.plot(x, y, 'ro', label='Original data')
#     # plt.plot(x, predictions, label='Fitted line')
#     # plt.title('Linear Regression Result')
#     # plt.legend()
#     # plt.show()


def execute_tler_for_pair(i, j, np_X, np_Y, data_W0, data_Wi, data_Wj):
    # Actual data
    x = np_X
    y = np_Y.T

    n = np.size(x, 0)
    m = np.size(x, 1)

    X = tf.placeholder("float64", shape=(m, None), name="X")
    Y = tf.placeholder("float64", shape=(1, None), name="Y")

    W0 = tf.Variable(initial_value=data_W0, name="W0", dtype="float64")
    Wi = tf.Variable(initial_value=data_Wi, name="Wi", dtype="float64")
    Wj = tf.Variable(initial_value=data_Wj, name="Wj", dtype="float64")
    # W0 = tf.Variable(np.random.randn(), name="W0", dtype="float64")
    # Wi = tf.Variable(np.random.randn(), name="Wi", dtype="float64")
    # Wj = tf.Variable(np.random.randn(), name="Wj", dtype="float64")
    # b = tf.Variable(np.random.randn(), name="b")

    # learning_rate = 0.01
    learning_rate = 0.01
    training_epochs = 100

    # Hypothesis
    y_pred = tf.multiply(X, tf.add(tf.add(W0, Wi), Wj))

    # print(y_pred)

    # Mean Squared Error Cost Function
    cost = tf.reduce_sum(tf.pow(y_pred - Y, 2)) / (2 * n)

    # Gradient Descent Optimizer
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    # Global Variables Initializer
    init = tf.global_variables_initializer()

    # Starting the Tensorflow Session
    with tf.Session() as sess:
        # Initializing the Variables
        sess.run(init)

        # Iterating through all the epochs
        for epoch in range(training_epochs):

            # Feeding each data point into the optimizer using Feed Dictionary
            for (_x, _y) in zip(x, y):
                _x_reshaped = np.array([_x]).T
                # _x_reshaped = _x
                _y_np = np.array([_y]).T.reshape(1, 1)
                # print("_y: " + str(_y.shape))
                # print("_y_np: " + str(_y_np.shape))
                sess.run(optimizer, feed_dict={X: _x_reshaped, Y: _y_np})

            # Displaying the result after every n epochs
            if (epoch + 1) % 10 == 0:
                x_reshaped = x.transpose()
                # x_reshaped = x
                y_reshaped = y.reshape(1, n)
                # y_reshaped = y
                # Calculating the cost a every epoch
                c = sess.run(cost, feed_dict={X: x_reshaped, Y: y_reshaped})
                # print("Epoch", (epoch + 1), ": cost =", c, "W0 =", sess.run(W0), "Wi =", sess.run(Wi), "Wj =", sess.run(Wj))
                # print("Epoch", (epoch + 1), ": cost =", c)

            # Storing necessary values to be used outside the Session
        x_reshaped = x.T
        y_reshaped = np.array([y])
        training_cost = sess.run(cost, feed_dict={X: x_reshaped, Y: y_reshaped})
        weight_W0 = sess.run(W0)
        weight_Wi = sess.run(Wi)
        weight_Wj = sess.run(Wj)

    # Calculating the predictions
    predictions = x.dot(weight_W0) + x.dot(weight_Wi) + x.dot(weight_Wj)
    # print("Training cost =", training_cost, "Weight_W0 =", weight_W0, "Weight_Wi =", weight_Wi, "Weight_Wj =", weight_Wj,'\n')
    # print("Training cost =", training_cost, '\n')

    # # Plotting the Results
    # plt.plot(x, y, 'ro', label='Original data')
    # plt.plot(x, predictions, label='Fitted line')
    # plt.title('Linear Regression Result')
    # plt.legend()
    # plt.show()

    return weight_W0, weight_Wi, weight_Wj


def normalize_df(df):
    x = df.values  # returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler(feature_range=(-1.0, 1.0))
    x_scaled = min_max_scaler.fit_transform(x)
    normalized_df = pd.DataFrame(x_scaled, columns=df.columns)
    normalized_df.index = df.index
    return normalized_df


# Custom gradient: https://medium.com/datadriveninvestor/override-tensorflow-backward-propagation-1a7e2bb3f516



def execute_tler_tensorflow():
    workspace_path = "/data/projects/mdedup/workspace/testing_tensorflow/"
    # df_X_train, df_Y_train, df_X_test, df_Y_test, np_X_train, np_Y_train, np_X_test, np_Y_test = execute_tler()
    # df_X_train.to_pickle(workspace_path + "X_train.pkl")
    # df_Y_train.to_pickle(workspace_path + "Y_train.pkl")
    # df_X_test.to_pickle(workspace_path + "X_test.pkl")
    # df_Y_test.to_pickle(workspace_path + "Y_test.pkl")

    df_X_train = pd.read_pickle(workspace_path + "X_train.pkl")
    df_Y_train = pd.read_pickle(workspace_path + "Y_train.pkl")
    df_X_test = pd.read_pickle(workspace_path + "X_test.pkl")
    df_Y_test = pd.read_pickle(workspace_path + "Y_test.pkl")

    df_X_train = normalize_df(df_X_train)
    df_Y_train = normalize_df(df_Y_train)
    df_X_test = normalize_df(df_X_test)
    df_Y_test = normalize_df(df_Y_test)

    # Get pairs of sources
    sources = list(tuple(src) for src in np.unique(df_Y_train[["part1", "part2"]], axis=0))

    mode = "directly_connected_sources"

    m = len(df_X_train.columns)

    np.random.seed(101)
    tf.set_random_seed(101)
    # tf.random.set_seed(101)

    test_sources = [0, 1]
    counter_source_pair_combinations = 0
    if mode == "directly_connected_sources":
        group_by_ij = {}
        for source in sources:
            for tst_src in test_sources:
                if source[0] == tst_src or source[1] == tst_src:
                    other_source = source[0] if source[0] != tst_src else source[1]
                    if tst_src not in group_by_ij:
                        group_by_ij[tst_src] = []
                    group_by_ij[tst_src].append(other_source)
                    counter_source_pair_combinations += 1

        # idx_by_i = {}
        # def store_index(idx_by_i, i, df_Y_train, df_Y_test):
        #     i_idx_train = (df_Y_train["part1"] == i) | (df_Y_train["part2"] == i)
        #     # i_idx_test = (df_Y_test["part1"] == i) | (df_Y_test["part2"] == i)
        #     if i not in idx_by_i:
        #         idx_by_i[i] = {
        #             "train": i_idx_train,
        #             # "test": i_idx_test
        #         }
        #
        # df_by_i = {}
        # def store_data(idx_by_i, df_by_i, i, df_Y_train, df_Y_test):
        #     if i not in df_by_i:
        #         i_idx_train = idx_by_i[i]["train"]
        #         i_idx_test = idx_by_i[i]["test"]
        #         i_train = df_Y_train[i_idx_train]
        #         i_train.reset_index(inplace=True, drop=True)
        #
        #         # i_test = df_Y_test[i_idx_test]
        #         # i_test.reset_index(inplace=True, drop=True)
        #
        #         i_np_X_train = df_X_train[i_idx_train].to_numpy()
        #         i_np_Y_train = df_Y_train[i_idx_train]["class"].to_numpy()
        #         i_np_X_test = df_X_test[i_idx_test].to_numpy()
        #         i_np_Y_test = df_Y_test[i_idx_test]["class"].to_numpy()
        #
        #         df_by_i[i] = {
        #             # "train": i_train,
        #             # "test": i_test
        #             "X_train": i_np_X_train,
        #             "Y_train": i_np_Y_train,
        #             # "X_test": i_np_X_test,
        #             # "Y_test": i_np_Y_test,
        #         }

        w_by_i = {}

        W0 = np.random.randn(m).reshape(m, 1)
        tqdm()
        with tqdm(desc="Training", total=counter_source_pair_combinations) as pbar:
            for i, group in group_by_ij.items():
                # store_index(idx_by_i, i, df_Y_train, df_Y_test)
                # store_data(idx_by_i, df_by_i, i, df_Y_train, df_Y_test)

                # i_X_train = df_by_i[i]["X_train"]
                # i_Y_train = df_by_i[i]["Y_train"]

                Wi = w_by_i[i] if i in w_by_i else np.random.randn(m).reshape(m, 1)
                for j in group:
                    pbar.update()
                    # store_index(idx_by_i, j, df_Y_train, df_Y_test)
                    # store_data(idx_by_i, df_by_i, j, df_Y_train, df_Y_test)

                    # j_X_train = df_by_i[j]["X_train"]
                    # j_Y_train = df_by_i[j]["Y_train"]
                    ij_idx = (((df_Y_train["part1"] == i) & (df_Y_train["part2"] == j)) | ((df_Y_train["part1"] == j) & (df_Y_train["part2"] == i)))
                    ij_np_X_train = df_X_train[ij_idx].to_numpy()
                    ij_np_Y_train = df_Y_train[ij_idx]["class"].to_numpy()

                    Wj = w_by_i[j] if j in w_by_i else np.random.randn(m).reshape(m, 1)

                    W0, Wi, Wj = execute_tler_for_pair(i, j, ij_np_X_train, ij_np_Y_train, W0, Wi, Wj)

                    w_by_i[i] = Wi
                    w_by_i[j] = Wj

        #         print("hi2")
        # print("hi")

        i, j = test_sources[0], test_sources[1]
        ij_np_X_test = df_X_test.to_numpy()
        ij_np_Y_test = df_Y_test["class"].to_numpy()

        Wi = w_by_i[i]
        Wj = w_by_i[j]
        Wij = W0 + (1.0/2) * (Wi + Wj)

        predictions_ij = ij_np_X_test.dot(Wij)
        final_predictions_ij = np.sign(predictions_ij)

        print("hi3")
        # print(final_predictions_ij)
        # print(ij_np_Y_test)

        Y_test = ij_np_Y_test
        Y_pred = final_predictions_ij

        clf_report = precision_recall_fscore_support(Y_test, Y_pred, average="macro")
        precision, recall, fscore, support = clf_report
        print(clf_report)


    print(sources)
    exit(-1)



    # j_idx = (df_Y_train["part1"] == 1) | (df_Y_train["part2"] == 1)
    # j_train = df_Y_train[j_idx]
    # j_train.reset_index(inplace=True, drop=True)





if __name__ == '__main__':
    # execute()
    execute_tler_tensorflow()