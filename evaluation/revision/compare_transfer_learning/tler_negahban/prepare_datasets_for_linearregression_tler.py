import os

import editdistance
import pandas as pd
import pymysql as pymysql
from pyhocon import ConfigFactory
from sklearn.metrics import precision_recall_fscore_support, confusion_matrix, make_scorer
from sklearn.model_selection import StratifiedKFold
from sklearn.svm import LinearSVC
from tqdm import tqdm

from evaluation.revision.compare_transfer_learning.tler_negahban.convert_dataset_to_record_linkage import dd_to_rl

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CONF = ConfigFactory.parse_file(ROOT_DIR + '/resources/reference.conf')

"""
In this Python script we attempted to immitate the algorithm developed at:

Negahban, S. N., Rubinstein, B. I., & Gemmell, J. G. (2012, October). Scaling multiple-source entity resolution using 
statistically efficient transfer learning. In Proceedings of the 21st ACM international conference on Information and 
knowledge management (pp. 2224-2228). ACM.

In order to compare with Negahban et al.'s algorithm we had to create a custom scenario. The reason for this is that 
their system requires that different participating datasets to be from the same domain and schema. Therefore, we convert
all our datasets into this form. We split our datasets into six parts, in a way that duplicates are placed across the 
different parts and then we perform record linkage between them. We keep one of these pairs of parts to be used as our
test set. The rest can be used for training.

"execute_linearregression.py" performs the actual experiment, whereas the 
"prepare_datasets_for_linearregression_tler.py" prepares the datasets with the logic we previously explained. The 
execution uses TensorFlow to provide us with reasonable execution times. 
"""

def load_dataset(dataset):
    R = None
    if dataset == "amazon_walmart":

        database = "mdedup"
        db_connection = pymysql.connect(host=CONF["database_ip"],
                                        user=CONF["database_username"],
                                        password=CONF["database_password"],
                                        db=CONF["database_name"],
                                        # charset='utf8mb4',
                                        charset='utf8',
                                        cursorclass=pymysql.cursors.DictCursor)
        df = pd.read_sql("SELECT * FROM " + database + ".amazon_walmart", con=db_connection)

        R = df.astype(str)
        db_connection.close()
    else:
        R = pd.read_csv(CONF["data_dir"] + dataset + "_prepared" + ".tsv", delimiter="\t", keep_default_na=False, dtype=str)
        # R.index = R["id"]
    R.set_index(keys=["id"], drop=False, inplace=True)
    attributes = CONF["datasets"][dataset]["attributes"] + ["id"]
    R = R[attributes]
    R.index = R.index.astype(str)

    R_dict = R.to_dict(orient="index")

    # R_ids = set(R["id"].unique())
    R_ids = set(R["id"].unique())

    Y_dpl = pd.read_csv(CONF["data_dir"] + dataset + "_DPL.tsv", delimiter="\t", keep_default_na=False, dtype=str)
    Y_dpl["class"] = 1
    Y_ndpl = pd.read_csv(CONF["data_dir"] + dataset + "_NDPL.tsv", delimiter="\t", keep_default_na=False, dtype=str)
    Y_ndpl["class"] = 0
    Y = pd.concat([Y_dpl, Y_ndpl])
    Y.reset_index(inplace=True)

    print(len(Y))
    Y = Y[(Y["id1"].isin(R_ids)) & (Y["id2"].isin(R_ids))]
    Y.reset_index(inplace=True)
    print(len(Y))

    return R_dict, attributes, Y


def calculate_similarities(R, attributes, Y):
    X = []
    if "id" in attributes:
        attributes.remove("id")
    for idx, y in tqdm(Y.iterrows(), desc="Calculating similarities: ", total=len(Y)):
        id1 = str(y["id1"])
        id2 = str(y["id2"])
        if id1 not in R:  # This should only happen in amazon_walmart, where loading does not always work properly.
            print("ID: " + id1 + " not in R")
            continue
        if id2 not in R: # This should only happen in amazon_walmart, where loading does not always work properly.
            print("ID: " + id2 + " not in R")
            continue

        r2 = R[id2]
        r1 = R[id1]
        x = []
        for attr in sorted(attributes):
            v1 = r1[attr]
            v2 = r2[attr]
            # sim = textdistance.levenshtein.normalized_similarity(v1, v2)
            if len(v1) == 0 and len(v2) == 0:
                sim = 0.0
            else:
                sim = 1.0 - (editdistance.eval(v1, v2) / float(max(len(v1), len(v2))))
            x.append(sim)
        X.append(x)
    Xdf = pd.DataFrame.from_records(X, columns=sorted(attributes))
    return Xdf


def get_coefficients(dataset):
    R, attributes, Y = load_dataset(dataset)
    # Y = Y.sample(n=200)
    X = calculate_similarities(R, attributes, Y)
    clf = LinearSVC(random_state=0, tol=1e-5)
    Xarr = X.to_numpy()
    Yarr = Y["class"].to_numpy()

    clf.fit(Xarr, Yarr)
    return clf.coef_


def train_and_execute(X_train, Y_train, X_test, Y_test):
    skf = StratifiedKFold(n_splits=10)
    def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
    def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
    def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
    def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]

    # scoring = {
    #     'accuracy': 'accuracy',
    #     'recall': 'recall',
    #     'precision': 'precision',
    #     'roc_auc': 'roc_auc',
    #     'average_precision': 'average_precision',
    #     "f1_macro": "f1_macro",
    #     'tp': make_scorer(tp),
    #     'tn': make_scorer(tn),
    #     'fp': make_scorer(fp),
    #     'fn': make_scorer(fn)
    # }

    clf = LinearSVC(random_state=0, tol=1e-5)
    # result = cross_validate(clf, X_train, Y_train, cv=StratifiedKFold(n_splits=10), scoring=scoring)

    clf.fit(X_train, Y_train)
    Y_pred = clf.predict(X_test)

    clf_report = precision_recall_fscore_support(Y_test, Y_pred, average="macro")
    print(clf_report)


def execute_tler(dataset="restaurants"):
    sample_ratio = 1.0
    R, attributes, Y = load_dataset(dataset)

    Y = Y.sample(n=int(sample_ratio*len(Y)))
    X = calculate_similarities(R, attributes, Y)

    new_Y, record_to_cluster = dd_to_rl(R, X, Y)
    new_Y = new_Y.reset_index()
    new_X = calculate_similarities(R, attributes, new_Y)

    test_idx = (((new_Y["part1"] == 0) & (new_Y["part2"] == 1)) | ((new_Y["part1"] == 1) & (new_Y["part2"] == 0)))
    train_idx = ~test_idx

    new_X_train = new_X[train_idx]
    new_X_test = new_X[test_idx]
    new_Y_train = new_Y[train_idx]
    new_Y_test = new_Y[test_idx]

    np_X_train = new_X_train.to_numpy()
    np_Y_train = new_Y_train["class"].to_numpy()
    np_X_test = new_X_test.to_numpy()
    np_Y_test = new_Y_test["class"].to_numpy()
    train_and_execute(np_X_train, np_Y_train, np_X_test, np_Y_test)

    # X_arr = new_X.to_numpy()
    # Y_arr = new_Y["class"].to_numpy()

    print("hi")

    df_X_train = new_X_train
    df_Y_train = new_Y_train
    df_X_test = new_X_test
    df_Y_test = new_Y_test

    workspace_path = "/data/projects/mdedup/workspace/testing_tensorflow/datasets_parts/"
    save_dataset_parts(R, dataset, new_X, new_Y, record_to_cluster, workspace_path)

    return df_X_train, df_Y_train, df_X_test, df_Y_test, np_X_train, np_Y_train, np_X_test, np_Y_test, new_X, new_Y, R, record_to_cluster


def export_tler_dataset_parts():
    workspace_path = "/data/projects/mdedup/workspace/testing_tensorflow/datasets_parts/"

    dataset = "ncvoters"

    df_X_train, df_Y_train, df_X_test, df_Y_test, np_X_train, np_Y_train, np_X_test, np_Y_test, new_X, new_Y, R, record_to_cluster = execute_tler(dataset)
    save_dataset_parts(R, dataset, new_X, new_Y, record_to_cluster, workspace_path)


def save_dataset_parts(R, dataset, new_X, new_Y, record_to_cluster, workspace_path):
    df_R = pd.DataFrame.from_dict(R, orient="index")
    df_R["part_id"] = ""
    n_parts = 6
    last_part_assigned = 0
    for i, row in df_R.iterrows():
        # ifor_val = something
        # if < condition >:
        #     ifor_val = something_else
        id = row["id"]
        part_id = None

        if id in record_to_cluster:
            part_id = str(record_to_cluster[id])
        else:
            last_part_assigned += 1
            last_part_assigned = last_part_assigned % n_parts
            part_id = str(last_part_assigned)

        df_R.set_value(i, 'part_id', part_id)
    parts = new_Y[["part1", "part2"]]
    parts = parts.drop_duplicates(subset=["part1", "part2"])
    parts.sort_values(by=["part1", "part2"], inplace=True)
    new_Y["class"].replace(0, "ndpl", inplace=True)
    new_Y["class"].replace(1, "dpl", inplace=True)
    print(new_Y)
    for idx, (part1, part2) in parts.iterrows():
        parts_ids = (((new_Y["part1"] == part1) & (new_Y["part2"] == part2)) |
                     ((new_Y["part1"] == part2) & (new_Y["part2"] == part1)))

        parts_X = new_X[parts_ids]
        parts_Y = new_Y[parts_ids]

        ids1 = parts_Y["id1"].unique()
        ids2 = parts_Y["id2"].unique()
        ids = set()
        ids.update(ids1)
        ids.update(ids2)

        parts_R = df_R[df_R["id"].isin(ids)]

        parts_R.to_csv(workspace_path + dataset + "_" + str(part1) + "_" + str(part2) + "_R" + ".tsv", sep="\t",
                       index=False)
        parts_X.to_csv(workspace_path + dataset + "_" + str(part1) + "_" + str(part2) + "_X" + ".tsv", sep="\t",
                       index=False)
        parts_Y.to_csv(workspace_path + dataset + "_" + str(part1) + "_" + str(part2) + "_Y" + ".tsv", sep="\t",
                       index=False)


if __name__ == '__main__':
    # execute_tler()
    export_tler_dataset_parts()
