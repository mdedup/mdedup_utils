import itertools
import random

import pandas as pd
from tqdm import tqdm

from evaluation.revision.compare_transfer_learning.tler_negahban.unionfind.deehzee_unionfind import UnionFind


def cluster(R, X, Y):
    n = max(int(x) for x in R.keys())
    print(n)
    # uf = unionfind(n)  # There are n items.
    uf = UnionFind()
    for idx, y in tqdm(Y.iterrows(), desc="Union records", total=len(Y)):
        id1 = int(y["id1"])
        id2 = int(y["id2"])

        # uf.unite(id1, id2)
        uf.add(id1)
        uf.add(id2)
        uf.union(id1, id2)
    # uf_groups = uf.groups()
    uf_groups = uf.components()

    clusters = [list(x) for x in uf_groups]
    clusters_str = [[str(i) for i in x] for x in clusters]
    clusters_str.sort(key=lambda x: len(x), reverse=True)
    return clusters_str


def split_clusters_to_parts(clusters, n_parts):
    newgs = []
    record_to_cluster = {}

    starting_part = 0  # This variable ensures an equal distribution of records among the different parts.

    for cl in clusters:
        for i in range(len(cl)):
            record_to_cluster[cl[i]] = (i + starting_part) % n_parts
        combinations = list(itertools.combinations(cl, 2))
        for cmb in combinations:
            cluster_id1 = record_to_cluster[cmb[0]]
            cluster_id2 = record_to_cluster[cmb[1]]
            if cluster_id1 > cluster_id2:
                cluster_id1, cluster_id2 = cluster_id2, cluster_id1

            id1, id2 = cmb[0], cmb[1]
            if int(id1) > int(id2):
                id1, id2 = id2, id1

            label = (id1, id2, cluster_id1, cluster_id2, 1)
            newgs.append(label)
        starting_part += 1
    return newgs, record_to_cluster


def remove_labels_of_same_parts(parts):
    new_parts = []
    for p in parts:
        if p[2] == p[3]:
            continue
        new_parts.append(p)
    return new_parts


def introduce_non_duplicate_labels_random(labels, n_parts, record_to_cluster):
    gs = set()
    ids = set()
    for l in labels:
        id1 = l[0]
        id2 = l[1]
        if int(id1) > int(id2):
            id1, id2 = id2, id1
        gs.add(id1 + "_" + id2)
        ids.add(id1)
        ids.add(id2)
    ids = list(ids)
    ndpl = set()
    random.seed(a=0)
    while len(ndpl) < len(gs)*10:
        id1 = ids[random.randint(0, len(ids)-1)]
        id2 = ids[random.randint(0, len(ids)-1)]
        if id1==id2:
            continue
        if record_to_cluster[id1] == record_to_cluster[id2]:
            continue
        if int(id1) > int(id2):
            id1, id2 = id2, id1
        if id1 + "_" + id2 in gs:
            continue
        part1 = record_to_cluster[id1]
        part2 = record_to_cluster[id2]
        if part1 > part2:
            part1, part2 = part2, part1
        ndpl.add((id1, id2, part1, part2, 0))
    ndpl = list(ndpl)
    return ndpl


def dd_to_rl(R, X, Y):
    Y = Y[Y["class"] == 1]  # Keep only duplicates
    clusters = cluster(R, X, Y)
    print(clusters)
    n_parts = 6
    dpl, record_to_cluster = split_clusters_to_parts(clusters, n_parts)
    print(dpl)
    filtered_labels = remove_labels_of_same_parts(dpl)
    print(filtered_labels)
    ndpl = introduce_non_duplicate_labels_random(dpl, n_parts, record_to_cluster)
    # TODO: apply BLOCKING
    print(ndpl)
    print("hi")

    newYdpl = pd.DataFrame().from_records(data=dpl, columns=["id1", "id2", "part1", "part2", "class"])
    newYndpl = pd.DataFrame().from_records(data=ndpl, columns=["id1", "id2", "part1", "part2", "class"])

    newY = pd.concat([newYdpl, newYndpl])

    print(newY)
    return newY, record_to_cluster
