import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn import preprocessing
from sklearn.metrics import precision_recall_fscore_support
from tqdm import tqdm

from evaluation.revision.compare_transfer_learning.tler_negahban.prepare_datasets_for_linearregression_tler import execute_tler

"""
In this Python script we attempted to immitate the algorithm developed at:

Negahban, S. N., Rubinstein, B. I., & Gemmell, J. G. (2012, October). Scaling multiple-source entity resolution using 
statistically efficient transfer learning. In Proceedings of the 21st ACM international conference on Information and 
knowledge management (pp. 2224-2228). ACM.

In order to compare with Negahban et al.'s algorithm we had to create a custom scenario. The reason for this is that 
their system requires that different participating datasets to be from the same domain and schema. Therefore, we convert
all our datasets into this form. We split our datasets into six parts, in a way that duplicates are placed across the 
different parts and then we perform record linkage between them. We keep one of these pairs of parts to be used as our
test set. The rest can be used for training.

"execute_linearregression.py" performs the actual experiment, whereas the 
"prepare_datasets_for_linearregression_tler.py" prepares the datasets with the logic we previously explained. The 
execution uses TensorFlow to provide us with reasonable execution times. 
"""

def custom_loss_function(X, Y, W0, Wi, Wj, n, tau):
    # Hypothesis
    y_pred = tf.multiply(X, tf.add(tf.add(W0, Wi), Wj))
    # Mean Squared Error Cost Function
    cost = tf.reduce_sum(tf.pow(y_pred - Y, 2)) / (2 * n)
    return cost


def execute_tler_for_dataset_part_pair(i, j, np_X, np_Y, data_W0, data_Wi, data_Wj):
    tau = 0.2
    # tau = tf.constant(0.2)

    # Actual data
    x = np_X
    y = np_Y.T

    n = np.size(x, 0)
    m = np.size(x, 1)

    # n = tf.constant(np.size(x, 0))

    X = tf.compat.v1.placeholder("float64", shape=(m, None), name="X")
    Y = tf.compat.v1.placeholder("float64", shape=(1, None), name="Y")

    W0 = tf.Variable(initial_value=data_W0, name="W0", dtype="float64")
    Wi = tf.Variable(initial_value=data_Wi, name="Wi", dtype="float64")
    Wj = tf.Variable(initial_value=data_Wj, name="Wj", dtype="float64")
    # b = tf.Variable(np.random.randn(), name="b")

    learning_rate = 0.01
    training_epochs = 100

    # graph = tf.Graph()
    cost = custom_loss_function(X, Y, W0, Wi, Wj, n, tau)
    # cost = add_grad(X, Y, W0, Wi, Wj, n, tau, graph)
    # cost = add_grad(W0, Wi, Wj, n, tau, graph)

    # Gradient Descent Optimizer
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
    # optimizer = tf.compat.v1.train.GradientDescentOptimizer(learning_rate).minimize(cost)
    # optimizer = tf.contrib.optimizer_v2.GradientDescentOptimizer(learning_rate).minimize(cost)

    # Global Variables Initializer
    init = tf.global_variables_initializer()

    # Starting the Tensorflow Session
    with tf.Session() as sess:
        # Initializing the Variables
        sess.run(init)

        # Iterating through all the epochs
        for epoch in range(training_epochs):

            # Feeding each data point into the optimizer using Feed Dictionary
            for (_x, _y) in zip(x, y):
                _x_reshaped = np.array([_x]).T
                # _x_reshaped = _x
                _y_np = np.array([_y]).T.reshape(1, 1)
                # print("_y: " + str(_y.shape))
                # print("_y_np: " + str(_y_np.shape))
                sess.run(optimizer, feed_dict={X: _x_reshaped, Y: _y_np})

            # Displaying the result after every n epochs
            if (epoch + 1) % 10 == 0:
                x_reshaped = x.transpose()
                # x_reshaped = x
                y_reshaped = y.reshape(1, n)
                # y_reshaped = y
                # Calculating the cost a every epoch
                c = sess.run(cost, feed_dict={X: x_reshaped, Y: y_reshaped})
                # print("Epoch", (epoch + 1), ": cost =", c, "W0 =", sess.run(W0), "Wi =", sess.run(Wi), "Wj =", sess.run(Wj))
                # print("Epoch", (epoch + 1), ": cost =", c)

            # Storing necessary values to be used outside the Session
        x_reshaped = x.T
        y_reshaped = np.array([y])
        training_cost = sess.run(cost, feed_dict={X: x_reshaped, Y: y_reshaped})
        weight_W0 = sess.run(W0)
        weight_Wi = sess.run(Wi)
        weight_Wj = sess.run(Wj)

    # Calculating the predictions
    predictions = x.dot(weight_W0) + x.dot(weight_Wi) + x.dot(weight_Wj)
    # print("Training cost =", training_cost, "Weight_W0 =", weight_W0, "Weight_Wi =", weight_Wi, "Weight_Wj =", weight_Wj,'\n')

    return weight_W0, weight_Wi, weight_Wj


def normalize_df(df):
    x = df.values  # returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler(feature_range=(-1.0, 1.0))
    x_scaled = min_max_scaler.fit_transform(x)
    normalized_df = pd.DataFrame(x_scaled, columns=df.columns)
    normalized_df.index = df.index
    return normalized_df


"""
Failed attempts to immitate the soft-thresholding operator.
"""
# Custom gradient: https://medium.com/datadriveninvestor/override-tensorflow-backward-propagation-1a7e2bb3f516
# @tf.RegisterGradient("Sub")
# def _sub_grad(unused_op, grad):
#   return grad, tf.negative(grad)
# def add_grad(X, Y, W0, Wi, Wj, n, tau, graph, name=None):
# # def add_grad(W0, Wi, Wj, n, tau, graph, name=None):
#     with tf.name_scope(name, "AddGrad", [W0, Wi, Wj, n, tau]) as name:
#         return numpy_function(forward_func,
#                         # np.array([X, Y, W0, Wi, Wj, n, tau]),
#                         [X, Y, W0, Wi, Wj, n, tau],
#                         [np.float64],
#                         graph,
#                         name=name,
#                         grad=backprop_func)
#
# # def py_func(func, inp, Tout, graph, stateful=True, name=None, grad=None):
# def numpy_function(func, inp, Tout, graph, stateful=True, name=None, grad=None):
#
#     # Need to generate a unique name to avoid duplicates
#     # if you have more than one custom gradients:
#     # rnd_name = 'PyFuncGrad'+ str(np.random.randint(0, 1E+8))
#     # rnd_name = 'NumpyFuncGrad'+ str(np.random.randint(0, 1E+8))
#     rnd_name = 'EagerFuncGrad'+ str(np.random.randint(0, 1E+8))
#
#     W0, Wi, Wj, X, Y, n, tau = inp
#
#     tf.RegisterGradient(rnd_name)(grad)
#     # with graph.gradient_override_map({"PyFunc": rnd_name}):
#     # with graph.gradient_override_map({"PyFunc": rnd_name}):
#     with graph.gradient_override_map({"EagerPyFunc": rnd_name}):
#         # return tf.py_function(func, inp, Tout, name=name)
#         # return tf.py_function(func, [inp], Tout, name=name)
#         return tf.py_function(func, np.array([X, Y, W0, Wi, Wj, n, tau]), Tout, name=name)
#         # return tf.numpy_function(func, inp, Tout, name=name)
#         # return tf.numpy_function(func, np.array(inp), Tout, name=name)
#
#
# # def forward_func(c, k, d):
# #     e = np.add(c, k)
# #     return e.astype(np.float32)
#
# def forward_func(X, Y, W0, Wi, Wj, n, tau):
# # def forward_func(W0, Wi, Wj, n, tau):
# # def forward_func(op):
# #     X, Y, W0, Wi, Wj, n, tau = list(op)
#     cost = custom_loss_function(X, Y, W0, Wi, Wj, n, tau)
#     return cost.astype(np.float64)
#
#
# # def backprop_func(op, grad):
# #     c = op.inputs[0]
# #     k = op.inputs[1]
# #     d = op.inputs[2]
# #     e = tf.add(c, k)
# #     return (e + d) * grad , d * grad, e * grad
#
# def soft_thresholding(W, tau):
#     nW = W
#     # for i in range(len(nW)):
#     #     v = nW[i]
#     #     nv = tau if abs(v) >= tau else 0.0
#     #     nW[i] = nv
#     return nW
#
# def backprop_func(op, grad):
#     W0, Wi, Wj, X, Y, n, tau = op
#
#     W0_upd = W0 - grad
#     Wi_upd = Wi - grad
#     Wi_upd_op = soft_thresholding(Wi_upd, tau)
#
#     return W0_upd, Wi_upd_op, Wj, X, Y, n, tau



def execute_tler_tensorflow():
    workspace_path = "/data/projects/mdedup/workspace/testing_tensorflow/"
    # dataset = "restaurants"
    classification_metrics_by_dataset = {}
    for dataset in ["restaurants", "cddb", "census", "amazon_walmart", "cora", "dblp_scholar", "ncvoters", "hotels"]:
    # for dataset in ["cddb"]:
        df_X_train, df_Y_train, df_X_test, df_Y_test, np_X_train, np_Y_train, np_X_test, np_Y_test, new_X, new_Y, \
                new_R, record_to_cluster = execute_tler(dataset=dataset)
        df_X_train.to_pickle(workspace_path + "X_train_"+dataset+".pkl")
        df_Y_train.to_pickle(workspace_path + "Y_train_"+dataset+".pkl")
        df_X_test.to_pickle(workspace_path + "X_test_"+dataset+".pkl")
        df_Y_test.to_pickle(workspace_path + "Y_test_"+dataset+".pkl")

        df_X_train = pd.read_pickle(workspace_path + "X_train_"+dataset+".pkl")
        df_Y_train = pd.read_pickle(workspace_path + "Y_train_"+dataset+".pkl")
        df_X_test = pd.read_pickle(workspace_path + "X_test_"+dataset+".pkl")
        df_Y_test = pd.read_pickle(workspace_path + "Y_test_"+dataset+".pkl")

        # attributes = CONF[dataset]

        df_X_train = normalize_df(df_X_train)
        # df_Y_train = normalize_df(df_Y_train)
        df_X_test = normalize_df(df_X_test)
        # df_Y_test = normalize_df(df_Y_test)

        df_Y_train["class"] = df_Y_train["class"].replace(0, -1)
        df_Y_test["class"] = df_Y_test["class"].replace(0, -1)

        # Get pairs of sources
        sources = list(tuple(src) for src in np.unique(df_Y_train[["part1", "part2"]], axis=0))

        mode = "directly_connected_sources"

        m = len(df_X_train.columns)

        np.random.seed(101)
        # tf.set_random_seed(101)
        tf.compat.v1.set_random_seed(101)
        # tf.set_random_seed(101)

        test_sources = [0, 1]
        counter_source_pair_combinations = 0
        # if mode == "directly_connected_sources":
        group_by_ij = {}
        for source in sources:
            for tst_src in test_sources:
                if source[0] == tst_src or source[1] == tst_src:
                    other_source = source[0] if source[0] != tst_src else source[1]
                    if tst_src not in group_by_ij:
                        group_by_ij[tst_src] = []
                    group_by_ij[tst_src].append(other_source)
                    counter_source_pair_combinations += 1

        w_by_i = {}

        W0 = np.random.randn(m).reshape(m, 1)
        tqdm()
        with tqdm(desc="Training", total=counter_source_pair_combinations) as pbar:
            for i, group in group_by_ij.items():
                Wi = w_by_i[i] if i in w_by_i else np.random.randn(m).reshape(m, 1)
                for j in group:
                    pbar.update()
                    ij_idx = (((df_Y_train["part1"] == i) & (df_Y_train["part2"] == j)) | ((df_Y_train["part1"] == j) & (df_Y_train["part2"] == i)))
                    ij_np_X_train = df_X_train[ij_idx].to_numpy()
                    ij_np_Y_train = df_Y_train[ij_idx]["class"].to_numpy()

                    Wj = w_by_i[j] if j in w_by_i else np.random.randn(m).reshape(m, 1)

                    W0, Wi, Wj = execute_tler_for_dataset_part_pair(i, j, ij_np_X_train, ij_np_Y_train, W0, Wi, Wj)

                    w_by_i[i] = Wi
                    w_by_i[j] = Wj

        #         print("hi2")
        # print("hi")

        i, j = test_sources[0], test_sources[1]
        ij_np_X_test = df_X_test.to_numpy()
        ij_np_Y_test = df_Y_test["class"].to_numpy()

        Wi = w_by_i[i]
        Wj = w_by_i[j]
        Wij = W0 + (1.0/2) * (Wi + Wj)

        predictions_ij = ij_np_X_test.dot(Wij)
        final_predictions_ij = np.sign(predictions_ij)

        # final_predictions_ij[final_predictions_ij == 1] = 0
        # final_predictions_ij[final_predictions_ij == -1] = 1

        # final_predictions_ij[final_predictions_ij == 1] = 0
        # final_predictions_ij[final_predictions_ij == -1] = 0

        print("hi3")
        # print(final_predictions_ij)
        # print(ij_np_Y_test)

        Y_test = ij_np_Y_test
        Y_pred = final_predictions_ij

        # clf_report = precision_recall_fscore_support(Y_test, Y_pred, average="macro")
        clf_report = precision_recall_fscore_support(Y_test, Y_pred, average="binary")
        precision, recall, fscore, support = clf_report
        manually_calculated = calculate_classification_metrics(Y_pred, Y_test)
        print(clf_report)
        print(dataset)
        print(manually_calculated)

        classification_metrics_by_dataset[dataset] = manually_calculated
        # print(sources)
        # exit(-1)
    print(str(classification_metrics_by_dataset))

def calculate_classification_metrics(Y_pred, Y_test):
    tp = 0
    fp = 0
    fn = 0
    tn = 0

    for idx, r_pred in np.ndenumerate(Y_pred):
        r_pred = int(r_pred)
        r_test = Y_test[idx[0]]
        if r_pred == 1 and r_test == 1:
            tp += 1
        elif r_pred == 1 and r_test == -1:
            fp += 1
        elif r_pred == -1 and r_test == 1:
            fn += 1
        elif r_pred == -1 and r_test == -1:
            tn += 1

    p = float(tp) / (tp + fp)
    r = float(tp) / (tp + fn)
    f1 = 2.0 * (r * p) / (r + p)

    return {
        "tp": tp,
        "fp": fp,
        "fn": fn,
        "tn": tn,
        "p": p,
        "r": r,
        "f1": f1
    }


if __name__ == '__main__':
    # execute()
    execute_tler_tensorflow()
    # from tensorflow.python.client import device_lib
    # print(device_lib.list_local_devices())