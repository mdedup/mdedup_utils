import glob
import sys

import matplotlib.pyplot as plt
import pandas as pd

from evaluation.revision.compare_transfer_learning.mdedup.datasets_importer_transfer_learning import \
    DatasetsImporterTLER
from mdedup import CONF
from steps.execute_boosting import MDCBoosting
from steps.explore_external import MDCExploration
from steps.hymd.execute_hymd import HyMDController
from steps.hymd.import_hymd_to_relation import HyMDImporter
from steps.hymd.mdedup_calculate_similarities import HyMDSimilaritiesCalculator
from steps.predict_external import MDCPrediction
from steps.selection_expansion_external import MDCSelectionExpansion


def execute_mdedup():
    dataset_to_compare = "ncvoters"
    dataset = dataset_to_compare
    dataset_parts_dir = "/data/projects/mdedup/workspace/testing_tensorflow/datasets_parts/"

    R_fpaths = glob.glob(dataset_parts_dir + dataset + "_*_*_R.tsv")
    Y_fpaths = glob.glob(dataset_parts_dir + dataset + "_*_*_Y.tsv")

    parts = []

    CONF["database_name"] = CONF["database_name_tler"]
    CONF["database_connection_url"] = CONF["database_connection_url_tler"]
    # CONF["hymd"]["configuration"]["connection"] = CONF["database_connection_url_tler"]
    CONF["data_dir"] = dataset_parts_dir
    for R_fpath in sorted(R_fpaths):
        import re
        # m = re.search('AAA(.+?)ZZZ', text)
        m = re.search(dataset_parts_dir + dataset + "_(.+?)_(.+?)_R.tsv", R_fpath)
        if m:
            part1 = m.group(1)
            part2 = m.group(2)
            parts.append(part1 + "_" + part2)
            # parts.append((part1, part2))
    # parts.sort()
    print(parts)

    part_datasets = [dataset + "_" + p for p in parts]
    CONF["datasets"] = part_datasets
    print(part_datasets)

    sample = {}
    for part_dataset in part_datasets:
        CONF["hymd"][part_dataset] = CONF["hymd"][dataset]
        sample[part_dataset] = 0.1

    # Step  1: Import datasets and gold standards
    di = DatasetsImporterTLER()
    # 1.a: One relation for every dataset
    di.convert_datasets_to_relations()
    # 1.b: All gold standards (duplicates and non-duplicates) of all datasets are merged into a single relation
    #   "pairs".
    di.convert_gold_standards_to_single_relation()

    # Step  2: MD Discovery
    hymd_controller = HyMDController()

    # 2.a: Execute
    hymd_controller.execute_hymd_commands(find_latest_hymd_output_file=True)

    current_path = sys.path[0]
    hymd_output_dir = current_path + "/gold/"

    CONF["hymd_output_dir"] = hymd_output_dir
    for dataset in CONF["datasets"]:
        CONF["hymd"][dataset]["hymd"] = dataset + ".txt"

    # 2.b: Import MDs
    imp = HyMDImporter()
    imp.import_hymd_to_mdedup_discover()

    # 2.c: Calculate similarities for all available column matches
    sc = HyMDSimilaritiesCalculator()
    sc.calculate_deduplication_similarities()

    # Step 3: Discover and expand Matching Dependency Combinations
    dgs = MDCSelectionExpansion()
    dgs.execute_selection_and_expansion(topk=16, sample=sample)

    # Step 4: Generate features for the discovered MDCs
    xplr = MDCExploration()
    xplr.execute_exploration(topk=16, sample=sample)

    # Step 5: Predict MDCs
    prd = MDCPrediction()
    prd.execute_prediction(topk=16, sample=sample, datasets_to_predict=[dataset_to_compare + "_0_1"])

    # Step 6: Apply boosting
    bst = MDCBoosting()
    bst.execute_boosting(topk=16, execute_on_mdedup_values=["prediction"], datasets_to_predict=[dataset_to_compare + "_0_1"])

    pass


def plot_tler():
    fpath = "/data/projects/mdedup/workspace/testing_tensorflow/datasets_parts/TF_0_1.xls"
    df = pd.read_excel(fpath)
    df["method"] = df["method"].replace("tler", "Negahban et al.")
    df.index = df[["dataset", "method"]]
    print(df)
    df = df.rename(columns={"method": "Transfer learning approach"})

    pd.pivot_table(df, index=["dataset"], columns='Transfer learning approach', values ='f1').plot(kind='bar')
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.set_xticklabels(labels=sorted(list(df["dataset"].unique())), rotation=45, ha="center")
    plt.ylabel("F-measure (F1)")
    plt.xlabel("")
    patches, labels = ax.get_legend_handles_labels()
    ax.legend(patches, labels, title="Transfer learning approach", loc='best')


    plt.show()
    pass


if __name__ == '__main__':
    # execute_mdedup()
    plot_tler()