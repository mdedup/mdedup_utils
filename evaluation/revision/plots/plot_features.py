import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pymysql
from numpy.core.defchararray import lower
import matplotlib.patheffects as path_effects

from mdedup import CONF


def plot_effect_features(topk, enabled_expansion_phase, plotmode = "leave-one-out", valuesmode="f1_normalized"):
    db_connection = pymysql.connect(host=CONF["database_ip"],
                                    user=CONF["database_username"],
                                    password=CONF["database_password"],
                                    db=CONF["database_name"],
                                    charset='utf8mb4',
                                    # charset='utf8',
                                    cursorclass=pymysql.cursors.DictCursor)
    sqlstr = None
    if plotmode == "leave-one-out":
        sqlstr = "SELECT * FROM mdedup_boosting_featureexperiments where " + "description='trained_on_mdedup_prediction' and " + "topk=" + \
                 str(topk) + " and "+ "enabled_expansion_phase=" + str(enabled_expansion_phase) + " and " + \
                "length(features_in_regression) >= length(tested_features_in_regression)"
    elif plotmode == "single-feature":
        sqlstr = "SELECT * FROM mdedup_boosting_featureexperiments where " + "description='trained_on_mdedup_prediction' and " + "topk=" + \
                 str(topk) + " and " + "enabled_expansion_phase=" + str(enabled_expansion_phase) + " and " + \
                 "length(features_in_regression) <= length(tested_features_in_regression)"

    df = pd.read_sql(sqlstr, con=db_connection, coerce_float=False)
    fig, ax = plt.subplots()

    df = df[["dataset", "features_in_regression", "tested_features_in_regression", "f1"]]

    # df["feature"] = ""

    def extract_feature(features_in_regression):
        feature = None
        if features_in_regression == "complete_feature_set":
            feature = "COMPLETE"
        else:
            feature = features_in_regression
        return feature


    df['feature'] = df.apply(lambda row: extract_feature(row["tested_features_in_regression" if plotmode == "leave-one-out" else "features_in_regression"]), axis=1)

    del df["features_in_regression"]
    print("hi")

    # In case of multiple Top-K's
    df = df.groupby(["dataset", "feature"]).agg({"f1": "max"}).reset_index()
    dfc = df.copy()

    # Calculate relative F-measure to the all feature experiment. [-1, 1]
    def calculate_relative_to_complete(row, dfc):
        complete_f1_row = dfc[(dfc["dataset"] == row["dataset"]) & (dfc["feature"] == "COMPLETE")]
        relative_diff = 0.0
        if len(complete_f1_row) > 0:
            complete_f1 = complete_f1_row["f1"].iloc[0]
            relative_diff = row["f1"] - complete_f1
        return relative_diff

    df['f1_relative'] = df.apply(lambda row: calculate_relative_to_complete(row, dfc), axis=1)

    # Calculate normalized F-measure [0, 1] using min and max values across dataset's experiments.
    dfgb_dataset = df.groupby(["dataset"]).agg({"f1": {"f1_max": "max", "f1_min": "min"}}).reset_index()
    def calculate_normalized_to_minmax(row, dfgb_dataset):
        minmax_f1_row = dfgb_dataset[dfgb_dataset["dataset"] == row["dataset"]]
        dataset_min = minmax_f1_row.iloc[0]["f1"]["f1_min"]
        dataset_max = minmax_f1_row.iloc[0]["f1"]["f1_max"]
        normalized_value = -2.0
        if dataset_min == dataset_max:
            normalized_value = 1.0
        elif len(minmax_f1_row) > 0:
            normalized_value = (row["f1"] - dataset_min) / (dataset_max - dataset_min)
        return normalized_value
    df['f1_normalized'] = df.apply(lambda row: calculate_normalized_to_minmax(row, dfgb_dataset), axis=1)

    _plot_effect_features_heatmap(df, plotmode=plotmode, valuesmode=valuesmode)
    _export_ordered_ranking_of_features(df, plotmode=plotmode)

def _print_effect_features(df):

    pass

def _plot_effect_features_bars(df):
    del df["f1"]
    df = df.drop(df[df["feature"] == "COMPLETE"].index)
    pd.pivot_table(df, index='feature', columns='dataset', values='f1_relative').plot(kind='barh')
    plt.show()

def _plot_effect_features_heatmap(df:pd.DataFrame, plotmode = "leave-one-out", valuesmode="f1_normalized"):
    def normalize_features(ft):
        ft = lower(ft)
        ft = np.char.replace(ft, "_", " ")
        return ft

    df["feature"] = df["feature"].apply(lambda ft: normalize_features(ft))

    datasets = sorted(set(df["dataset"]))
    features = sorted(set(df["feature"]))
    features.remove("complete")

    values = []
    for dataset in datasets:
        dataset_values = []
        for feature in features:
            r = df[(df["dataset"] == dataset) & (df["feature"] == feature)]
            v = 0.0
            try:
                v = r[valuesmode].iloc[0]
            except:
                # if plotmode != "leave-one-out":
                #     v = -10.0
                #     if dataset == "hotels":
                #         print("feature_to_calculate " + feature)
                pass
            dataset_values.append(v)
        values.append(dataset_values)

    values = np.array(values)

    fig, ax = plt.subplots(dpi=600, figsize=(8, 6))

    min_value, max_value = min(df[valuesmode]), max(df[valuesmode])
    max_of_abs = max(abs(min_value), max_value)

    def prettify_feature(v):
        v = lower(v).flat[0]
        v = v.replace("_", " ")
        return v

    feature_to_idx = {prettify_feature(ft): idx for idx, ft in enumerate(list(CONF["mdc_features"]))}
    features_numbered = [str(feature_to_idx[ft]) + " " + ft for i, ft in enumerate(features)]

    im = None
    if valuesmode == "f1_relative":
        cmap = "bwr"
        im, _ = heatmap(values, datasets, features_numbered, ax=ax, cmap=cmap,
                        cbarlabel="F-measure relative ["+plotmode+"]", vmin=-max_of_abs, vmax=max_of_abs)
    elif valuesmode == "f1_normalized":
        cmap = "binary"
        im, _ = heatmap(values, datasets, features_numbered, ax=ax, cmap=cmap,
                        cbarlabel="F-measure normalized ["+plotmode+"]", vmin=min_value, vmax=max_value)
    # im, _ = heatmap(values, datasets, features, ax=ax, cmap=cmap, cbarlabel="F-measure effect by leave-one-out")
    annotate_heatmap(im, valfmt="{x:.2f}", size=3, textcolors=["black", "black"], valuesmode=valuesmode)

    # def func(x, pos):
    #     return "{:.2f}".format(x).replace("0.", ".").replace("1.00", "")
    #
    # annotate_heatmap(im, valfmt=matplotlib.ticker.FuncFormatter(func), size=7)

    fig.tight_layout()
    plt.show()

    print("hi")


    pass


# https://matplotlib.org/3.1.1/gallery/images_contours_and_fields/image_annotated_heatmap.html
def heatmap(data, row_labels, col_labels, ax=None, cbar_kw={}, cbarlabel="", valuesmode="f1_normalized", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, aspect='auto', **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)
    # ax.set_xtick_params(fontsize=2)
    ax.xaxis.set_tick_params(labelsize=7)
    # ax.tick_params(axis='both', which='major', labelsize=2)
    # ax.tick_params(axis='both', which='minor', labelsize=2)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right", rotation_mode="anchor")
    # plt.setp(ax.get_xticklabels(), rotation=-90, ha="right", rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


# https://matplotlib.org/3.1.1/gallery/images_contours_and_fields/image_annotated_heatmap.html
def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, valuesmode="f1_normalized", **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            v = float(data[i, j])

            if v == -10.0:
                kw["color"] = 'w'
            elif v < 0.0:
                kw["fontweight"] = 'bold'
            else:
                kw["fontweight"] = 'regular'


            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            text.set_path_effects([path_effects.withStroke(linewidth=3, foreground='w')])
            texts.append(text)

    return texts

def _export_ordered_ranking_of_features(df, plotmode):
    df["f1_ranked"] = ""
    df_to_export = pd.DataFrame(columns=df.columns)
    for dataset, dfgroupedby in df.groupby(["dataset"]):
        dfgroupedby.sort_values("f1_normalized", inplace=True)
        # dfgroupedby["f1_ranked"] = dfgroupedby["f1_normalized"].rank(ascending=False).astype(int)
        # dfgroupedby["f1_ranked"] = dfgroupedby["f1_normalized"].rank(ascending=False, method="dense").astype(int)
        dfgroupedby["f1_ranked"] = dfgroupedby["f1_normalized"].rank(ascending=False, method="min").astype(int)
        print("hi")
        df_to_export = df_to_export.append(dfgroupedby)

    print(df)

    filename = CONF["workspace_dir"] + "features_comparison_"+plotmode.replace("-", "_") +".xlsx"
    df_to_export.to_excel(filename)