import pymysql

from mdedup import CONF
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def plot_effect_topk(enabled_expansion_phase, features_in_regression):
    db_connection = pymysql.connect(host=CONF["database_ip"],
                                    user=CONF["database_username"],
                                    password=CONF["database_password"],
                                    db=CONF["database_name"],
                                    charset='utf8mb4',
                                    # charset='utf8',
                                    cursorclass=pymysql.cursors.DictCursor)

    relation_name = "mdedup_boosting"

    df = pd.read_sql("SELECT * FROM mdedup_seschat."+relation_name+" where "
                     "description='trained_on_mdedup_prediction' and "
                     "enabled_expansion_phase=" + str(enabled_expansion_phase) + " and "
                                                                                 "features_in_regression='" + features_in_regression + "'",
                     con=db_connection,
                     coerce_float=False)

    fig, ax = plt.subplots()

    df = df[["dataset", "topk", "f1"]]
    df = df.groupby(["dataset", "topk"]).agg({"f1": "max"}).reset_index()

    dfgb = df.groupby(["dataset"])

    markers = [".", "x", "+", "|", "1", "4", 5, 2, 3]
    datasets = []
    handles = []
    for i, (dataset, dfgroupedby) in enumerate(dfgb):
        datasets.append(datasets)
        print(dfgroupedby)

        xy = dfgroupedby[["topk", "f1"]]

        h = ax.plot(xy["topk"], xy["f1"], label=dataset, marker=markers[i])
        handles.append(h)
    print(dfgb)

    for i, (dataset, dfgroupedby) in enumerate(df.groupby(["topk"])):
        print(dfgroupedby["f1"].mean())
    ax.set_ylabel("F-measure (F1)")
    ax.set_xlabel("k")
    ax.set_xscale('log', basex=2)
    plt.legend()
    plt.show()


def plot_effect_expansion(topk, features_in_regression):
    db_connection = pymysql.connect(host=CONF["database_ip"],
                                    user=CONF["database_username"],
                                    password=CONF["database_password"],
                                    db=CONF["database_name"],
                                    charset='utf8mb4',
                                    # charset='utf8',
                                    cursorclass=pymysql.cursors.DictCursor)

    relation_name = "mdedup_boosting"

    df = pd.read_sql("SELECT * FROM mdedup_seschat."+relation_name+" where "
                     "description='trained_on_mdedup_prediction' and "
                     "topk=" + str(topk) + " and "
                                           "features_in_regression='" + features_in_regression + "'", con=db_connection,
                     coerce_float=False)

    fig, ax = plt.subplots()

    df = df[["dataset", "enabled_expansion_phase", "f1"]]

    datasets = sorted(df["dataset"].unique())
    ind = np.arange(len(datasets))  # the x locations for the groups
    width = 0.35  # the width of the bars

    expansion_phase_values = []
    handles = []
    for i, (enabled_expansion_phase, dfgroupedby) in enumerate(df.groupby(["enabled_expansion_phase"])):
        expansion_phase_values.append("True" if  enabled_expansion_phase == 1 else "False")
        print(dfgroupedby)

        xy = dfgroupedby[["dataset", "f1"]]

        xygrouped = xy.groupby(["dataset"]).agg({"f1": "max"}).reset_index()

        datasets_in_experiment = set(xygrouped["dataset"].unique())
        missing_datasets = set(datasets) - datasets_in_experiment

        for dataset in missing_datasets:
            xygrouped.loc[len(xygrouped)] = [dataset, 0.0]
        xygrouped.sort_values(by=["dataset"], inplace=True)

        h = ax.bar(ind + i * width, xygrouped["f1"], width=width,
                   label="Enabled" if enabled_expansion_phase == 1 else "Disabled")

        handles.append(h)
    datasets = sorted(tuple(df["dataset"].unique()))

    n_groups = len(df.groupby(["enabled_expansion_phase"]))
    ax.set_xticks(ind + width / n_groups)
    ax.set_xticklabels(datasets, rotation = 45, ha="center")

    ax.legend().set_title("Expansion phase")
    ax.set_ylabel("F-measure (F1)")
    plt.show()