import matplotlib.pyplot as plt
import pandas as pd
import pymysql

from evaluation.revision.plots.plot_features import plot_effect_features
from evaluation.revision.plots.plot_topk_expansion import plot_effect_topk, plot_effect_expansion
from mdedup import CONF
from steps.execute_boosting import MDCBoosting
from steps.explore_external import MDCExploration
from steps.predict_external import MDCPrediction
from steps.selection_expansion_external import MDCSelectionExpansion


def experiment(topk_values, enabled_expansion_phase, mdc_features, enabled_experiment_topk,
               enabled_experiment_expansion, enabled_experiment_features, sample, apply_cpulimit):
    for topk in topk_values:
        # if enabled_experiment_topk or enabled_experiment_expansion:
        # Step 3: Discover and expand Matching Dependency Combinations
        dgs = MDCSelectionExpansion()
        # dgs.execute_selection_and_expansion(topk=topk, enabled_expansion_phase=enabled_expansion_phase, sample=sample, apply_cpulimit=apply_cpulimit)
        dgs.execute_selection_and_expansion(topk=topk, enabled_expansion_phase=True, sample=sample, apply_cpulimit=apply_cpulimit)

        # Step 4: Generate features for the discovered MDCs
        xplr = MDCExploration()
        xplr.execute_exploration(topk=topk, enabled_expansion_phase=enabled_expansion_phase, sample=sample, apply_cpulimit=apply_cpulimit)

        for i, l1o_features in enumerate(mdc_features):
            l1o_features_str = "|".join(l1o_features)
            if l1o_features_str == "complete_feature_set":
                tested_features_str = l1o_features_str
            else:
                all_features = set(CONF["mdc_features"])
                tested_features = all_features - set(l1o_features)
                tested_features_str = "|".join(tested_features)

            print("Tested features:" + tested_features_str)

            # Step 5: Predict MDCs
            prd = MDCPrediction()
            # prd.execute_prediction(topk=topk, enabled_expansion_phase=enabled_expansion_phase,
            #                        features_in_regression=l1o_features_str,
            #                        tested_features_in_regression=tested_features_str,
            #                        sample=sample, apply_cpulimit=apply_cpulimit)

            # Step 6: Apply boosting
            bst = MDCBoosting()
            bst.execute_boosting(topk=topk, enabled_expansion_phase=enabled_expansion_phase,
                                 features_in_regression=l1o_features_str,
                                 tested_features_in_regression=tested_features_str,
                                 # execute_on_mdedup_values=["prediction"],
                                 execute_on_mdedup_values=["gs"],
                                 sample=sample, apply_cpulimit=apply_cpulimit)


def plot_final_experiments_boosting():
    db_connection = pymysql.connect(host=CONF["database_ip"],
                                    user=CONF["database_username"],
                                    password=CONF["database_password"],
                                    db=CONF["database_name"],
                                    charset='utf8mb4',
                                    # charset='utf8',
                                    cursorclass=pymysql.cursors.DictCursor)

    sqlstr = "SELECT * FROM mdedup_boosting"
    df = pd.read_sql(sqlstr, con=db_connection, coerce_float=False)
    fig, ax = plt.subplots()
    df["features_in_regression"] = df["features_in_regression"].replace("cardinality|completeness|confidence|conviction|lift|set_size_ATTRIBUTE_INTERSECTION|set_size_ATTRIBUTE_JACCARD|set_size_ATTRIBUTE_MAX|set_size_ATTRIBUTE_MEDIAN|set_size_ATTRIBUTE_MIN|set_size_ATTRIBUTE_STDEV|set_size_ATTRIBUTE_UNION|set_size_COLUMN_MATCH_INTERSECTION|set_size_COLUMN_MATCH_JACCARD|set_size_COLUMN_MATCH_MAX|set_size_COLUMN_MATCH_MEDIAN|set_size_COLUMN_MATCH_MIN|set_size_COLUMN_MATCH_STDEV|set_size_COLUMN_MATCH_UNION|set_size_SIMILARITY_MEASURE_INTERSECTION|set_size_SIMILARITY_MEASURE_JACCARD|set_size_SIMILARITY_MEASURE_MAX|set_size_SIMILARITY_MEASURE_MEDIAN|set_size_SIMILARITY_MEASURE_MIN|set_size_SIMILARITY_MEASURE_STDEV|set_size_SIMILARITY_MEASURE_UNION|set_size_THRESHOLD_INTERSECTION|set_size_THRESHOLD_JACCARD|set_size_THRESHOLD_MAX|set_size_THRESHOLD_MEDIAN|set_size_THRESHOLD_MIN|set_size_THRESHOLD_STDEV|set_size_THRESHOLD_UNION|support|uniqueness", "wout thresholds")
    pd.pivot_table(df, index=["dataset", "description", "tested_features_in_regression"], columns="tested_features_in_regression", values='f1').plot(kind='bar')
    pass
    
if __name__ == '__main__':
    apply_cpulimit = True # False
    # Configure which experiment to execute.
    enabled_experiment_topk = False
    enabled_experiment_expansion = False
    enabled_experiment_features = False

    sample = None

    # Configure parameter values to use.
    topk_values = [16]
    enabled_expansion_phase = not (enabled_experiment_expansion)

    # mdc_features = []
    # if enabled_experiment_features:
    #     for i, feature in enumerate(CONF["mdc_features"]):
    #         # complete_mdc_features = copy.deepcopy(CONF["mdc_features"])
    #         # del complete_mdc_features[i]
    #         mdc_features.append([CONF["mdc_features"][i]])
    # else:
    #     mdc_features.append(["complete_feature_set"])

    # mdc_features = copy.deepcopy(CONF["mdc_features"])
    # mdc_features.remove("thresholds_min")
    # mdc_features.remove("thresholds_median")
    # mdc_features.remove("thresholds_max")
    # mdc_features = [mdc_features]

    mdc_features = [["complete_feature_set"]]

    # # Execute the experiment.
    experiment(topk_values, enabled_expansion_phase, mdc_features,
               enabled_experiment_topk=enabled_experiment_topk,
               enabled_experiment_expansion=enabled_experiment_expansion,
               enabled_experiment_features=enabled_experiment_features,
               sample=sample, apply_cpulimit=apply_cpulimit
    )

    # Plot the results of TOPK.
    plot_effect_topk(False, "complete_feature_set")

    # Plot the results of EXPANSION.
    plot_effect_expansion(16, "complete_feature_set")

    # Plot the results of FEATURES.
    plot_effect_features(16, True, plotmode="leave-one-out", valuesmode="f1_normalized")
    plot_effect_features(16, True, plotmode="leave-one-out", valuesmode="f1_relative")
    plot_effect_features(16, True, plotmode="single-feature", valuesmode="f1_normalized")
    plot_effect_features(16, True, plotmode="single-feature", valuesmode="f1_relative")