from collections import OrderedDict

import pandas as pd
import sqlalchemy

from evaluation.plot_mdcs_boosting_classification import export_plot
from mdedup import CONF


def gather_data():
    data = []
    group_by_order = None
    features_to_gather = ["dataset", "approach", "ids", "predicted_score", "classification_best_parameters", "tp", "fp", "tn", "fn", "f1", "recall",
                          "precision_", "execution_time"]
    db_connection = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'
                                             .format(CONF["database_username"],
                                                     CONF["database_password"],
                                                     CONF["database_ip"],
                                                     # CONF["database_name"]
                                                     "mdedup_seschat"
                                             )
                                             )

    datasets_order = ["census", "restaurants", "dblp_scholar", "cddb", "hotels", "amazon_walmart", "cora", "ncvoters"]
    for dataset in datasets_order:
        experiment = OrderedDict()

        topk_statement = " and topk=16 "

        prediction_features = "cardinality|completeness|confidence|conviction|lift|set_size_ATTRIBUTE_INTERSECTION|" \
                              "set_size_ATTRIBUTE_JACCARD|set_size_ATTRIBUTE_MAX|set_size_ATTRIBUTE_MEDIAN|" \
                              "set_size_ATTRIBUTE_MIN|set_size_ATTRIBUTE_STDEV|set_size_ATTRIBUTE_UNION|" \
                              "set_size_COLUMN_MATCH_INTERSECTION|set_size_COLUMN_MATCH_JACCARD|set_size_COLUMN_MATCH_MAX|" \
                              "set_size_COLUMN_MATCH_MEDIAN|set_size_COLUMN_MATCH_MIN|set_size_COLUMN_MATCH_STDEV|" \
                              "set_size_COLUMN_MATCH_UNION|set_size_SIMILARITY_MEASURE_INTERSECTION|" \
                              "set_size_SIMILARITY_MEASURE_JACCARD|set_size_SIMILARITY_MEASURE_MAX|" \
                              "set_size_SIMILARITY_MEASURE_MEDIAN|set_size_SIMILARITY_MEASURE_MIN|" \
                              "set_size_SIMILARITY_MEASURE_STDEV|set_size_SIMILARITY_MEASURE_UNION|set_size_THRESHOLD_INTERSECTION|" \
                              "set_size_THRESHOLD_JACCARD|set_size_THRESHOLD_MAX|set_size_THRESHOLD_MEDIAN|set_size_THRESHOLD_MIN|" \
                              "set_size_THRESHOLD_STDEV|set_size_THRESHOLD_UNION|support|uniqueness"

        experiment["MDC Selection"] = "select * from mdedup_mdcs where dataset='" + dataset + "' and `phase`='selection' "+topk_statement+" order by f1 desc limit 1;"
        experiment["MDC Prediction"] = "select * from mdedup_mdcs where dataset='" + dataset + "' and `phase`='prediction' "+topk_statement+\
                                       " and features_in_regression<>'" + "complete_feature_set" + "' " +\
                                       " order by prediction_score desc limit 1;"
        experiment["MDC Selection with boosting"] = "select * from mdedup_boosting where " \
                                                    "description='trained_on_mdedup_exploration' and " \
                                                    "dataset='" + dataset + "' "+topk_statement+" order by f1 desc limit 1;"
        experiment["MDC Prediction with boosting"] = "select * from mdedup_boosting where " \
                                                     "description='trained_on_mdedup_prediction' and " \
                                                     " features_in_regression <> '" + "complete_feature_set" + "' and " \
                                                     "dataset='" + dataset + "' "+topk_statement+" order by f1 desc limit 1;"
        for k, v in experiment.items():
            try:
                df = pd.read_sql(v, con=db_connection, coerce_float=False).iloc[0]
                df["approach"] = k
            except:
                continue
            dataset_data = []
            for f in features_to_gather:
                dataset_data.append(df[f] if f in df else "")
            data.append(dataset_data)
        group_by_order = list(experiment.keys())

    fix_precision_name(features_to_gather)

    return data, features_to_gather, group_by_order, datasets_order


def fix_precision_name(features_to_gather):
    i = features_to_gather.index("precision_")
    features_to_gather.remove("precision_")
    features_to_gather.insert(i, "precision")


def export_data(fpath, data, header):
    df = pd.DataFrame(data, columns=header)
    df.to_csv(path_or_buf=fpath, sep="\t", index=False)
    pass

def execute():
    data, header, group_by_order, datasets_order = gather_data()
    fpath = CONF["workspace_dir"] + "mdcs_boosting_classification.csv"
    export_data(fpath, data, header)
    fpath_pdf_base = CONF["workspace_dir"] + "mdcs_boosting_classification"
    export_plot(fpath_pdf_base, data, header, group_by_order, datasets_order)

if __name__ == '__main__':
    execute()
