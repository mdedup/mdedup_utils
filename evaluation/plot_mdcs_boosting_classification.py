import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pylab

from evaluation.mdedup_dataset_statistics import dataset_prettify

prettify = {
    "f1": "F-measure",
    "precision": "Precision",
    "recall": "Recall"
}


def export_plot(fpath_pdf_base, data, header, group_by_order, datasets_order):
    df_original = pd.DataFrame(data, columns=header)
    df = df_original

    groups = list(reversed(datasets_order))
    group_by = group_by_order
    group_by_pretified = group_by
    group_by_reversed = list(reversed(group_by))
    N = len(groups)

    colors = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99"]
    colors = list(reversed(colors[:len(group_by)]))
    hatches = ["/", "\\", "+", "x", "*"]  # ('-', '+', 'x', '\\', '*', 'o', 'O', '.', '/')
    hatches = list(reversed(hatches[:len(group_by)]))

    for metric in ["recall", "f1", "precision"]:
        if metric == "recall":
            font = {
                'size': 18.5,
            }
        else:
            font = {
                'size': 18.5,
            }

        plt.clf()
        plt.cla()

        plt.rc('font', **font)
        fig, ax = plt.subplots()

        ind = pd.np.arange(N)  # the x locations for the groups
        width = 0.2

        df_pivoted = df.pivot("dataset", "approach", metric)
        df_pivoted = df_pivoted.loc[groups, :]
        df_pivoted = df_pivoted.fillna(0.0)
        bars = {}
        for i, gb in enumerate(group_by_reversed):
            group_values = tuple(df_pivoted[gb])
            bars[gb] = ax.barh(ind + i * width, group_values, width,
                               color=colors[i],
                               hatch=hatches[i],
                               alpha=1.0,
           )
            # bars[gb] = ax.barh(ind + i * width, group_values, width, color=colors[i], hatch=hatches[i])
            autolabel(bars[gb], ax, metric)

        ax.set_ylim(-0.2, 7.8)
        ax.set_xlim(0.0, 1.0)

        ax.set_yticklabels([])
        plt.legend(frameon=False)

        if metric == "recall":
            fig.set_size_inches(cm2inch(30, 35))

            ax.set_yticks(ind + (5.0 * width) / 3.36)
            ax.set_yticklabels([dataset_prettify[g] for g in groups])
        else:
            fig.set_size_inches(cm2inch(25, 35))

        # Hide the right spines
        ax.spines['right'].set_visible(False)

        plt.tight_layout()

        fig.savefig(fpath_pdf_base + "_" + metric + ".pdf", dpi = 80, pad_inches=0, transparent=True, bbox_inches="tight")
        if metric == "recall":
            for legend_type in ["vertical", "horizontal"]:
                if legend_type == "vertical":
                    ax.legend((bars[g][0] for g in group_by), tuple(group_by_pretified),
                              bbox_to_anchor = (-0.025, 1.23),
                              loc="upper left",  # loc="upper right",
                              prop={'size': font["size"]},
                    )

                    fig_leg = plt.figure(figsize=(5, 2.5))
                    ax_leg = fig_leg.add_subplot(111)

                    ax_leg.legend(
                        (bars[g][0] for g in group_by),
                        tuple(group_by_pretified),
                        loc='center',
                    )
                    # hide the axes frame and the x/y labels
                    ax_leg.axis('off')
                    fig_leg.tight_layout()
                    fig_leg.savefig(fpath_pdf_base + '_legend_' + legend_type + '.pdf', dpi=80, pad_inches=0, transparent=True,
                                    bbox_inches="tight")
                elif legend_type == "horizontal":
                    def export_legend(legend, filename=fpath_pdf_base + "_legend_horizontal.pdf", expand=[-5, -5, 5, 5]):
                        fig = legend.figure
                        fig.canvas.draw()
                        bbox = legend.get_window_extent()
                        bbox = bbox.from_extents(*(bbox.extents + np.array(expand)))
                        bbox = bbox.transformed(fig.dpi_scale_trans.inverted())
                        fig.savefig(filename, dpi="figure", bbox_inches=bbox)

                    legend = ax.legend((bars[g][0] for g in group_by), tuple(group_by_pretified),
                              bbox_to_anchor=(-0.025, 1.23),
                              loc="upper left",  # loc="upper right",
                              prop={'size': font["size"]},
                              ncol=len(group_by)
                    )

                    export_legend(legend)

                    figlegend = pylab.figure(figsize=(15, 0.7))
                    figlegend.legend((bars[g][0] for g in group_by), tuple(group_by_pretified), ncol=len(group_by), mode = "expand")
                    figlegend.savefig(fpath_pdf_base + '_legend_'+legend_type+'.png')
                    figlegend.tight_layout()
                    figlegend.savefig(fpath_pdf_base + '_legend_' +legend_type+ '.pdf', dpi=80, pad_inches=0, transparent=True)


def autolabel(rects, ax, metric):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        width = rect.get_width()
        x = rect.get_x()
        y = rect.get_y()
        if metric == "recall":
            ax.text(1*width + 0.06,
                    y + 0.02,
                    '%.2f' % round(width, 2), ha='center', va='bottom',
                    # fontsize = "smaller"
                    fontsize=17
                    )
        else:
            ax.text(1 * width + 0.06,
                    y + 0.02,
                    '%.2f' % round(width, 2), ha='center', va='bottom',
                    # fontsize = "smaller"
                    fontsize=17
                    )


def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i / inch for i in tupl[0])
    else:
        return tuple(i / inch for i in tupl)