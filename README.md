# MDedup - Duplicate Detection with Matching Dependencies

This project is a utility that helps to automate the execution of [mdedup](https://gitlab.com/mdedup/mdedup), which is the main project that implements the steps described in the paper "Duplicate Detection with Matching Dependencies".

## Installation

This project requires Python to be executed. For compatibility reasons we have uploaded the anaconda packages we have used in the **resources/environment.yml**. Please import it into your own environment, before attempting to execute the scripts.
Such an import can be achieved using: `conda env create -f environment.yml`

## Execution

* The execution can be started using the mdedup.py script file. 
* Additional datasets can be added using the resources/reference.conf file. It is a HOCON configuration file, which allows users to easily add multi-leve configurations in an YML equivalent-way.
* Discovery of MDs is performed using [this tool](https://github.com/HPI-Information-Systems/metanome-algorithms/tree/master/HyMD). Its execution is handled by this project.


## Changes of revision

The main changes over the previous version are the comparison with _related work_ and experiments regarding selection of value _k_ (top-k), effect of the _expansion phase_, and finally effect of _MDC features_ in the prediction and boosting phase.

### Related work
We compared our approach with "Negahban, S. N., Rubinstein, B. I., & Gemmell, J. G. (2012, October). Scaling multiple-source entity resolution using statistically efficient transfer learning. In Proceedings of the 21st ACM international conference on Information and knowledge management (pp. 2224-2228). ACM.".
In order to compare with Negahban et al.'s algorithm we had to create a custom scenario. The reason for this is that 
their system requires that different participating datasets to be from the same domain and schema. Therefore, we convert
all our datasets into this form. We split our datasets into six parts, in a way that duplicates are placed across the 
different parts and then we perform record linkage between them. We keep one of these pairs of parts to be used as our
test set. The rest can be used for training.

* evaluation/revision/compare_transfer_learning/execute_linearregression.py: performs the actual experiment. The 
execution uses TensorFlow to provide us with reasonable execution times and robust machine learning models. 
* prepare_datasets_for_linearregression_tler.py: prepares the datasets with the logic we previously explained.

Our experimental results concluded that Negahban's approach is better for this scenario. In most of the datasets, as 
shown in the following figure, it is preferrable to select their approach instead of MDedup. This is expected as our 
approach has to go through the step of matching dependencies, which inevitably sets a higher bound on the best achievable
F-measure. The only exception is Cora, where the linear model is not able to distinguish properly the majority of the pairs.
![](evaluation/revision/compare_transfer_learning/tler_negahban/mdedup_comparison_with_negahban_tler.png)

### Experiments regarding top-k, expansion phase, and features
We experimented with different values of top-k, the effect of the expansion phase, and finally the effect of the different MDC features have in MDC prediction.
All these experiments can be found under evaluation/revision/experiment_and_plot.py.