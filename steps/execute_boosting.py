import os

import pymysql

from mdedup import CONF


class MDCBoosting:
    database_ip = CONF["database_ip"]
    database_name = CONF["database_name"]
    database_username = CONF["database_username"]
    database_password = CONF["database_password"]

    cpulimit_power = 1600

    def __init__(self):
        pass

    def execute_boosting(self, topk=16, enabled_expansion_phase=True, features_in_regression="complete_feature_set",
                         tested_features_in_regression = "N/A",
                         execute_on_mdedup_values=["null", "gs", "prediction"], sample=None, apply_cpulimit=False,
                         datasets_to_predict=sorted(CONF["datasets"])):
        for dataset in datasets_to_predict:
            for classification_algorithm in ["SUPPORT_VECTOR_MACHINES"]:
                for execute_on_mdedup in execute_on_mdedup_values:
                    print("boosting", dataset, topk, enabled_expansion_phase, sample[dataset] if sample is not None else None, execute_on_mdedup, classification_algorithm, features_in_regression)
                    print("Dataset: " + dataset + " classification: " + classification_algorithm + " execute_on_mdedup: " + str(execute_on_mdedup))
                    self._execute_boosting_from_java(dataset, topk, enabled_expansion_phase, features_in_regression,
                                                     tested_features_in_regression,
                                                     classification_algorithm, execute_on_mdedup,
                                                     sample[dataset] if sample is not None else None,
                                                     apply_cpulimit)


    def _execute_boosting_from_java(self, dataset, topk, enabled_expansion_phase, features_in_regression,
                                    tested_features_in_regression,
                                    classification_algorithm, execute_on_mdedup, sample, apply_cpulimit):
        jar = CONF["mdedup_jar"]
        database = CONF["database_name"]

        params = {
            "operation": "boosting",
            "dataset": dataset,
            "all_datasets": "amazon_walmart,cddb,census,cora,dblp_scholar,hotels,ncvoters,restaurants",

            "attributes": ",".join(CONF["hymd"][dataset]["attributes"]),
            "unsupervised": "true",
            "classification_algorithm": classification_algorithm,

            "topk": str(topk),
            "enabled_expansion_phase": str(enabled_expansion_phase),
            "features_in_regression": "'" + features_in_regression + "'",
            "tested_features_in_regression": "'" + tested_features_in_regression + "'",

            "db_user": CONF["database_username"],
            "db_password": CONF["database_password"],
            "db_driver": CONF["database_driver"],
            "db_connection": "'" + CONF["database_connection_url"] + "'",
            "db_relation": CONF["boosting_predict"]["relation"],
            # "sql_gs_sims": "\"" + "select * from sims_" + dataset + "\"", #(" where RAND(1) < " + str(sample) if sample is not None else "") + "\"", # DON'T include!
            "sql_gs_sims": "\"" + "select * from sims_" + dataset + (" order by RAND(1) < (select count(*)/"+str(sample)+" from "+database+".sims_"+dataset+")" if sample is not None else "") + "\"", # DON'T include!
            "sql_mdedup_prediction_pairs": "null",
        }
        if execute_on_mdedup == "null":
            if dataset == "hotels":
                hotels_sample = 0.2
                params["sql_gs_sims"] = "\"" + "select * from sims_" + dataset  + " where rand(1) < " + str(hotels_sample) + "\""
        elif execute_on_mdedup == "gs": # pairs produced by MDedup but using the Gold Standard
            params["sql_mdedup_prediction_pairs"] = "\"" + "select * from mdedup_classificationpairs WHERE dataset='" + dataset + "' and classification_algorithm='mdedup_selection'" + "\""
            params["description"] = "_exploration"
        elif execute_on_mdedup == "prediction":  # pairs produced by MDedup but using the ML regression prediction model
            params["sql_mdedup_prediction_pairs"] = "\"" + "select * from mdedup_classificationpairs WHERE dataset='" + dataset + "' and classification_algorithm='mdedup_prediction'" \
                    " and topk=" + str(topk) + " and enabled_expansion_phase=" + str(enabled_expansion_phase) + " and features_in_regression='" + features_in_regression + "'\""
            params["description"] = "_prediction"

        if apply_cpulimit:
            cmd = "cpulimit -f -l "+str(self.cpulimit_power)+" -- java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join([k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])
        else:
            cmd = "java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join([k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])
        print(cmd)
        os.system(cmd)


if __name__ == '__main__':
    cmt = MDCBoosting()
    cmt.execute_boosting()


