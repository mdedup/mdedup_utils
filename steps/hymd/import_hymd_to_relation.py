import os
import re

import pandas as pd
import sqlalchemy

from mdedup import CONF


class HyMDImporter:

    def import_hymd_to_mdedup_discover(self):
        for i, dataset in enumerate(CONF["datasets"]):
            if dataset not in CONF["hymd"]:
                print("Dataset: " + dataset + " not included in configuration")
                continue

            fpath = CONF["hymd_output_dir"] + CONF["hymd"][dataset]["hymd"]
            if not os.path.exists(fpath):
                continue
            with open(fpath, "rt") as fin:
                lines = fin.readlines()
            mds_set = set()
            mds = []
            for line in lines:
                lhs_list, rhs_list, support = self.parse_md(line)
                if len(lhs_list) == 0:
                    continue
                _, lhs_s, rhs_s, support = self._encode_md_to_string(lhs_list, rhs_list, support)
                if (dataset, lhs_s, rhs_s) not in mds_set:
                    mds_set.add((dataset, lhs_s, rhs_s))
                    mds.append((dataset, lhs_s, rhs_s, support))
            df = pd.DataFrame(mds, columns=["dataset", "lhs", "rhs", "support"])

            print(df)
            df["id"] = df.index.values.astype(str)

            db_con = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'
                                              .format(CONF["database_username"], CONF["database_password"],
                                                      CONF["database_ip"], CONF["database_name"]))

            df.to_sql(con=db_con, name='mdedup_hymd', if_exists="append", index=False, chunksize=1000)

    def parse_md(self, line):
        lhs, rhs = line.split("->")

        # Pattern to extract elements from LHS or RHS
        part_p = re.compile(r"\[(.*)\]\((.*)\)@(.*)")

        # LHS - extract elements
        lhs_list = []
        for part in lhs[1:-1].split(","):
            if part == "":
                print("EMPTY LHS")
                continue
            attribute, measure, threshold = self._extract_from_lhs_or_rhs(part, part_p)
            lhs_list.append((attribute, measure, threshold))
        print(lhs_list)

        # RHS - extract support
        p = re.compile(r"\(support=(.*)\)")
        m = p.search(rhs)

        support = m.group(1)
        rhs_clean = rhs[:m.start(0)]

        # RHS - extract elements
        rhs_list = [self._extract_from_lhs_or_rhs(rhs_clean, part_p)]

        return lhs_list, rhs_list, support

    def _extract_from_lhs_or_rhs(self, part, part_p):
        m = part_p.search(part)
        attribute, measure, threshold = m.group(1).split(".")[1].strip(), m.group(2).strip(), m.group(3).strip()

        threshold = str(round(float(threshold), 2))

        return attribute, measure, threshold

    def _encode_md_to_string(self, lhs_list, rhs_list, support):
        lhs_s = "#".join(["-".join(x) for x in lhs_list])
        rhs_s = "#".join(["-".join(x) for x in rhs_list])

        md_encoded = "|".join([lhs_s, rhs_s, support])

        return md_encoded, lhs_s, rhs_s, support

if __name__ == '__main__':
    imp = HyMDImporter()
    imp.import_hymd_to_mdedup_discover()