import os
import subprocess
import sys

from mdedup import CONF

class HyMDController:

    def execute_hymd_commands(self, find_latest_hymd_output_file=False):
        for dataset in CONF["datasets"]:
            print("Executing HyMD for dataset: " + dataset)
            cmd = self.create_hymd_command_for_dataset(dataset)
            print(cmd)
            # os.system(cmd)
            # out = os.popen(cmd).read()
            # out = subprocess.check_output(cmd, shell=True)
            proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
            (out, err) = proc.communicate()
            print(out)
            if find_latest_hymd_output_file:
                current_path = sys.path[0]
                import re
                # m = re.search('AAA(.+?)ZZZ', text)
                out_str = str(out)
                # m = re.search(current_path + "/gold/" + "(.+?).txt", out_str)
                m = re.search("gold/" + "(.+?).txt", out_str)
                if m:
                    hashcode = m.group(1)
                    old = current_path + "/gold/" + hashcode + ".txt"
                    new = current_path + "/gold/" + dataset + ".txt"
                    os.rename(old, new)


    def create_query_for_dataset(self, dataset):
        attributes = CONF["hymd"][dataset]["attributes"]
        limit = CONF["hymd"][dataset]["limit"]
        query = "select " + ", ".join(attributes) + " from " + dataset
        if limit != "None" and limit is not None:
            query += " order by rand(1) limit " + str(limit)
        return query


    def create_hymd_command_for_dataset(self, dataset):
        # CONF_HYMD = CONF["hymd"]["configuration"]

        vm_parameters = CONF["java_vm_parameters"]
        jar = CONF["hymd_jar"]
        user = CONF["database_username"]
        password = CONF["database_password"]
        query = self.create_query_for_dataset(dataset)
        connection = '"' + CONF["database_connection_url"] + '"'
        driver = CONF["database_driver"]
        config = CONF["hymd_config"]
        if "custom_configs" in CONF and dataset in CONF["custom_configs"]:
            config = CONF["custom_configs"][dataset]

        cmd = "java " + vm_parameters
        cmd += " -jar " + jar
        cmd += " --user " + user
        cmd += " --password " + password
        cmd += " --query \"" + query + "\""
        cmd += " --connection " + connection
        cmd += " --driver " + driver
        cmd += " --config " + config
        cmd += " --parallel"
        cmd += " --cache"

        return cmd


if __name__ == '__main__':
    hymd_controller = HyMDController()
    hymd_controller.execute_hymd_commands()
