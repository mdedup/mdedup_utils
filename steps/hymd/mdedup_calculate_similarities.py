import pandas as pd
import pymysql
import sqlalchemy
import textdistance
from parmap import parmap

from mdedup import CONF


class HyMDSimilaritiesCalculator:


    def calculate_for_pair(self, i, pair, data_dict, attribute_measures):
        id1, id2 = pair["id1"], pair["id2"]

        if id1 not in data_dict or id2 not in data_dict:
            print("Pair: " + id1, id2 + " not present in data")
            return None

        r = {
            "id": i,
            "id1": id1,
            "id2": id2,
            "class": pair["class"]
        }

        sims = []
        for am in sorted(attribute_measures):
            attribute, measure = am.split("-")
            msr = measures[measure]

            sim = msr(data_dict[id1][attribute], data_dict[id2][attribute])

            r[am] = sim

            sims.append(sim)

        sorted(sims)
        r["total_median"] = sims[int(len(sims) / 2)]
        r["total_mean"] = sum(sims) / float(len(sims))

        return r


    def calculate_for_dataset(self, dataset, df_hymd, df_data, df_dpl, df_ndpl):
        pairs = pd.concat([df_dpl, df_ndpl], ignore_index=True)

        attribute_measures = set()
        for i, r in df_hymd.iterrows():
            mds = list(r["lhs"].split("#"))
            mds.extend(r["rhs"].split("#"))
            for md in mds:
                if len(md.split("-")) == 3:
                    attribute, measure, _ = md.split("-")
                    if attribute in ["id", "original_id", "merged_id"]:
                        continue
                    attribute_measures.add(attribute + "-" + measure)

        header = set()
        header.update({"id", "id1", "id2", "class"})
        header.update(attribute_measures)
        header.update({"total_mean", "total_median"})

        data_dict = df_data.to_dict(orient="index")

        values_as_list = []
        for i, r in pairs.iterrows():
            values_as_list.append((i, r))

        sims_total = {}

        results = parmap.starmap(self.calculate_for_pair, values_as_list, data_dict=data_dict, attribute_measures=attribute_measures, pm_pbar=True, pm_processes=8)
        for r in results:
            if r is None:
                continue
            sims_total[r["id"]] = r

        df_sims = pd.DataFrame.from_dict(sims_total, columns=sorted(list(header)), orient="index")

        db_con = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'
                                          .format(CONF["database_username"], CONF["database_password"],
                                                  CONF["database_ip"], CONF["database_name"]))
        df_sims.to_sql(con=db_con, name='sims_' + dataset, if_exists='replace', index=False, chunksize=10000)


    def calculate_deduplication_similarities(self):
        database = CONF["database_name"]
        for dataset in CONF["datasets"]:
            db_connection = pymysql.connect(host=CONF["database_ip"],
                                            user=CONF["database_username"],
                                            password=CONF["database_password"],
                                            db=CONF["database_name"],
                                            # charset='utf8mb4',
                                            charset='utf8',
                                            cursorclass=pymysql.cursors.DictCursor)
            df_hymd = pd.read_sql("SELECT * FROM "+database+".mdedup_hymd WHERE dataset='" + dataset + "'", con=db_connection)

            if len(df_hymd) == 0:
                print("Empty HyMD")
                continue

            df_data = pd.read_sql("SELECT * FROM " + dataset, con=db_connection, coerce_float=False)
            df_data = df_data.where((pd.notnull(df_data)), None)
            df_data = df_data.astype(str)
            df_data.set_index(keys=["id"], inplace=True, drop=False)
            df_data.index = df_data.index.map(str)

            df_dpl = pd.read_sql("SELECT * FROM "+database+".pairs WHERE class=\"dpl\" and dataset='" + dataset + "'", con=db_connection).astype(str)
            df_ndpl = pd.read_sql("SELECT * FROM "+database+".pairs WHERE class=\"ndpl\" and dataset='" + dataset + "'", con=db_connection).astype(str)

            db_connection.close()

            print("Calculating for dataset: " + dataset)
            self.calculate_for_dataset(dataset, df_hymd, df_data, df_dpl, df_ndpl)


def empty_checks(s1, s2):
    if (s1 == "" or s1 is None) and (s2 == "" or s2 is None):
        return 1.0
    elif (s1 == "" or s1 is None) or (s2 == "" or s2 is None):
        return 0.0
    else:
        return None


def normalized_levenshtein(s1, s2):
    sim = empty_checks(s1, s2)
    if sim is None:
        sim = 1.0 - (float(textdistance.levenshtein(s1, s2)) / float(max(len(s1), len(s2))))
    return sim

monge_elkan_levenshtein = textdistance.MongeElkan(algorithm=textdistance.Levenshtein)

def normalized_monge_elkan_levenshtein(s1, s2):
    sim = empty_checks(s1, s2)
    if sim is None:
        return 1.0 - (monge_elkan_levenshtein(s1, s2) / float(max(len(s1), len(s2))))
    return sim

def normalized_jaccard(s1, s2):
    sim = empty_checks(s1, s2)
    if sim is None:
        # return 1.0 - (textdistance.jaccard(set(s1.split(" ")), set(s2.split(" "))))
        s1_toks = set(s1.split(" "))
        s2_toks = set(s2.split(" "))
        toks_intersection = s1_toks.intersection(s2_toks)
        toks_union = s1_toks.union(s2_toks)
        return float(len(toks_intersection))/len(toks_union)

    return sim

def equal(s1, s2):
    if s1 == s2:
        return 1.0
    else:
        return 0.0

measures = {
    "Levenshtein": normalized_levenshtein,
    "MongeElkan": normalized_monge_elkan_levenshtein,
    "Jaccard": normalized_jaccard,
    "Equal": equal
}

if __name__ == '__main__':
    sc = HyMDSimilaritiesCalculator()
    sc.calculate_deduplication_similarities()

