import os

from mdedup import CONF


class MDCExploration:
    database_username = CONF["database_username"]
    database_password = CONF["database_password"]
    database_ip = CONF["database_ip"]
    database_name = CONF["database_name"]

    cpulimit_power = 1600

    def __init__(self):
        pass

    def execute_exploration(self, topk=4, enabled_expansion_phase=True, sample=None, apply_cpulimit=False):
        for dataset in sorted(CONF["datasets"]):
            print("exploration", dataset, topk, enabled_expansion_phase, sample[dataset] if sample is not None else None)
            self.execute_exploration_from_java(dataset, topk, enabled_expansion_phase,
                                               sample[dataset] if sample is not None else None, apply_cpulimit)

    def execute_exploration_from_java(self, dataset, topk, enabled_expansion_phase, sample, apply_cpulimit):
        jar = CONF["mdedup_jar"]
        database = CONF["database_name"]
        params = {
            "operation": "exploration",
            "dataset": dataset,

            "query_records": "select * from " + dataset,
            "query_sims": "select * from "+database+".sims_" + dataset + (" where RAND(1) < " + str(sample) if sample is not None else ""),
            "query_hymd": "select * from "+database+".mdedup_hymd where dataset=\'" + dataset + "\'", #+ (" and RAND(1) < " + str(sample) if sample is not None else ""),
            "query_mdcs_selection": "select * from "+database+".mdedup_mdcs where phase='selection' and dataset=\'" + dataset + "\' and topk="+str(topk)+"",
            "query_mdcs_expansion": "select * from "+database+".mdedup_mdcs where phase='expansion' and dataset=\'" + dataset + "\' and topk="+str(topk)+"",

            "db_user": CONF["database_username"],
            "db_password": CONF["database_password"],
            "db_driver": CONF["database_driver"],
            "db_connection": "'" + CONF["database_connection_url"] + "'",

            "db_relation": CONF["exploration"]["relation"]
        }
        # sample_hymd = 1.0
        # if sample_hymd is not None and dataset in ["amazon_walmart"]:
        #     params["query_hymd"] += " and RAND(1) < " + str(0.4)
        # if sample_hymd is not None and dataset in ["ncvoters"]:
        #     params["query_hymd"] += " and RAND(1) < " + str(0.2)
        # else:
        #     if sample is not None:
        #         params["query_hymd"] += " and RAND(1) < " + str(sample)

        if apply_cpulimit:
            cmd = "cpulimit -f -l "+str(self.cpulimit_power)+" -- java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join(
                [k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])
        else:
            cmd = "java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join([k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])

        print(cmd)
        os.system(cmd)


if __name__ == '__main__':
    xplr = MDCExploration()
    xplr.execute_exploration()