import os

from mdedup import CONF


class MDCSelectionExpansion:
    database_ip = CONF["database_ip"]
    database_name = CONF["database_name"]
    database_username = CONF["database_username"]
    database_password = CONF["database_password"]

    def __init__(self):
        pass

    cpulimit_power = 1600


    def execute_selection_with_gs_from_java(self, dataset, topk, enabled_expansion_phase, max_duration, sample,
                                            apply_cpulimit):
        jar = CONF["mdedup_jar"]

        database = CONF["database_name"]

        params = {
            "operation": "selection",
            "dataset": dataset,
            "query_hymd": "select * from " + database + ".mdedup_hymd where dataset=\'" + dataset + "\'",
            "query_sims": "select * from " + database + ".sims_" + dataset + (" where RAND(1) < " + str(sample) if sample is not None else ""),

            "topk": str(topk),
            "enabled_expansion_phase": str(enabled_expansion_phase),

            "max_duration": str(max_duration),
            "db_user": CONF["database_username"],
            "db_password": CONF["database_password"],
            "db_driver": CONF["database_driver"],
            "db_connection": '"' + CONF["database_connection_url"] + '"',
        }

        # sample_hymd = 1.0
        # if sample_hymd is not None and dataset in ["amazon_walmart"]:
        #     params["query_hymd"] += " and RAND(1) < " + str(0.4)
        # if sample_hymd is not None and dataset in ["ncvoters"]:
        #     params["query_hymd"] += " and RAND(1) < " + str(0.2)
        # else:
        #     if sample is not None:
        #         params["query_hymd"] += " and RAND(1) < " + str(sample)

        if apply_cpulimit:
            cmd = "cpulimit -f -l "+str(self.cpulimit_power)+" -- java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join(
                [k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])
        else:
            cmd = "java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join([k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])

        print(cmd)
        os.system(cmd)

    def execute_expansion_with_gs_from_java(self, dataset, topk, sample_size_by_strategy, sample, apply_cpulimit):
        jar = CONF["mdedup_jar"]
        database = CONF["database_name"]
        params = {
            "operation": "expansion",
            "dataset": dataset,
            "query_hymd": "select * from "+database+".mdedup_hymd where dataset=\'" + dataset + "\'",
            "query_sims": "select * from "+database+".sims_" + dataset + (" where RAND(1) < " + str(sample) if sample is not None else ""),
            "topk": str(topk),
            "sample_size_by_strategy": '#'.join(sample_size_by_strategy),
            "db_user": CONF["database_username"],
            "db_password": CONF["database_password"],
            "db_driver": CONF["database_driver"],
            "db_connection": '"' + CONF["database_connection_url"] + '"',
        }

        # sample_hymd = 1.0
        # if sample_hymd is not None and dataset in ["amazon_walmart"]:
        #     params["query_hymd"] += " and RAND(1) < " + str(0.4)
        # elif sample_hymd is not None and dataset in ["ncvoters"]:
        #     params["query_hymd"] += " and RAND(1) < " + str(0.2)
        # else:
        #     if sample is not None:
        #         params["query_hymd"] += " and RAND(1) < " + str(sample)

        if apply_cpulimit:
            cmd = "cpulimit -f -l "+str(self.cpulimit_power)+" -- java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join(
                [k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])
        else:
            cmd = "java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join([k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])

        print(cmd)
        os.system(cmd)

    def execute_selection_and_expansion(self, topk=4, enabled_expansion_phase=True, sample=None, apply_cpulimit=False):
        for dataset in sorted(CONF["datasets"]):
            print("selection", dataset, topk, enabled_expansion_phase, sample[dataset] if sample is not None else None)
            self.execute_selection_with_gs_from_java(dataset, topk=topk, enabled_expansion_phase=enabled_expansion_phase,
                                                     max_duration=36000,
                                                     sample=sample[dataset] if sample is not None else None,
                                                     apply_cpulimit=apply_cpulimit)
            if enabled_expansion_phase:
                self.execute_expansion_with_gs_from_java(dataset, topk=topk,
                                                         sample_size_by_strategy=["gaussian_neighborhood-0.5", "uniform_random-0.5"],
                                                         sample=sample[dataset]  if sample is not None else None,
                                                         apply_cpulimit=apply_cpulimit)

if __name__ == '__main__':
    dgs = MDCSelectionExpansion()
    dgs.execute_selection_and_expansion()
