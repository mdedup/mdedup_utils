import os

from mdedup import CONF


class MDCPrediction:
    database_username = CONF["database_username"]
    database_password = CONF["database_password"]
    database_ip = CONF["database_ip"]
    database_name = CONF["database_name"]

    cpulimit_power = 1600

    def __init__(self):
        pass

    def execute_prediction(self, topk=4, enabled_expansion_phase=True, features_in_regression="complete_feature_set",
                           tested_features_in_regression="N/A", sample=None, apply_cpulimit=False,
                           datasets_to_predict=sorted(CONF["datasets"])):
        # for dataset in sorted(CONF["datasets"]):
        for dataset in datasets_to_predict:
            print("prediction", dataset, topk, enabled_expansion_phase, sample[dataset] if sample is not None else None, features_in_regression)
            self.execute_prediction_from_java(dataset, topk=topk, enabled_expansion_phase=enabled_expansion_phase,
                                              features_in_regression=features_in_regression,
                                              tested_features_in_regression=tested_features_in_regression,
                                              max_duration=36000,
                                              sample=sample[dataset] if sample is not None else None, apply_cpulimit=apply_cpulimit)

    def execute_prediction_from_java(self, dataset, topk, enabled_expansion_phase, features_in_regression,
                                     tested_features_in_regression, max_duration, sample, apply_cpulimit):
        jar = CONF["mdedup_jar"]

        database = CONF["database_name"]

        query_mdcs_exploration = "select * from "+database+".mdedup_mdcs where phase='exploration' and topk=" + str(topk) + " and " + "dataset <> '" + dataset + "'"

        params = {
            "operation": "prediction",
            "dataset": dataset,

            "query_records": "select * from " + dataset,
            "query_sims": "select * from "+database+".sims_" + dataset + (" order by RAND(1) < (select count(*)/"+str(sample)+" from "+database+".sims_"+dataset+")" if sample is not None else ""),
            "query_hymd": "select * from "+database+".mdedup_hymd where dataset=\'" + dataset + "\'", # + (" and RAND(1) < " + str(sample) if sample is not None else ""),

            "query_mdcs_selection": "select * from "+database+".mdedup_mdcs where phase='selection' and topk=" + str(topk),
                                    #" and enabled_expansion_phase=" + str(enabled_expansion_phase),
            "query_mdcs_expansion": "select * from "+database+".mdedup_mdcs where phase='expansion' and topk=" + str(topk),
            "query_mdcs_exploration": query_mdcs_exploration,
            "topk": str(topk),
            "enabled_expansion_phase": str(enabled_expansion_phase),
            "features_in_regression": "'" + features_in_regression + "'",
            "tested_features_in_regression": "'" + tested_features_in_regression + "'",

            # "max_level": "1",
            "max_duration": str(max_duration),

            "regression_model": "gaussian_process",

            "db_user": CONF["database_username"],
            "db_password": CONF["database_password"],
            "db_driver": CONF["database_driver"],
            "db_connection": "'" + CONF["database_connection_url"] + "'",

            "db_relation_out": CONF["prediction"]["relation"],
            "export_classified_pairs_by": "null"
        }
        if not enabled_expansion_phase:
            params["query_mdcs_expansion"] = "SELECT NULL LIMIT 0"

        # sample_hymd = 1.0
        # if sample_hymd is not None and dataset in ["amazon_walmart"]:
        #     params["query_hymd"] += " and RAND(1) < " + str(0.8)
        # elif sample_hymd is not None and dataset in ["ncvoters"]:
        #     params["query_hymd"] += " and RAND(1) < " + str(0.4)
        # else:
        #     if sample is not None:
        #         params["query_hymd"] += " and RAND(1) < " + str(sample)

        if apply_cpulimit:
            cmd = "cpulimit -f -l "+str(self.cpulimit_power)+" -- java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join(
                [k + "==" + (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])
        else:
            cmd = "java " + CONF["java_vm_parameters"] + " -jar " + jar + " " + " ".join([k + "==" +
                                    (v if not k.startswith("query") else "\"" + v + "\"") for k, v in params.items()])

        print(cmd)
        os.system(cmd)


if __name__ == '__main__':
    prd = MDCPrediction()
    prd.execute_prediction()