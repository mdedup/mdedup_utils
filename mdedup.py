import os

from pyhocon import ConfigFactory

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CONF = ConfigFactory.parse_file(ROOT_DIR + '/resources/reference.conf')


class MDedupController:
    """
    This utility allows automating the execution of the MDedup system implemented in Java.

    MDedup : Duplicate Detection with Matching Dependencies

    """

    def execute(self):
        from steps.execute_boosting import MDCBoosting
        from steps.explore_external import MDCExploration
        from steps.hymd.execute_hymd import HyMDController
        from steps.hymd.import_hymd_to_relation import HyMDImporter
        from steps.hymd.mdedup_calculate_similarities import HyMDSimilaritiesCalculator
        from steps.predict_external import MDCPrediction
        from steps.selection_expansion_external import MDCSelectionExpansion
        from datasets.import_datasets_to_relations import DatasetsImporter

        print("hi")
        exit(1)

        # Exploration experiment
        exploration_base = "selection" # selection, both

        # Leave-one-out feature experiment.
        leave_one_out_features = False

        # Step  1: Import datasets and gold standards
        di = DatasetsImporter()
        # 1.a: One relation for every dataset
        di.convert_datasets_to_relations()
        # 1.b: All gold standards (duplicates and non-duplicates) of all datasets are merged into a single relation
        #   "pairs".
        di.convert_gold_standards_to_single_relation()

        # Step  2: MD Discovery
        hymd_controller = HyMDController()

        # 2.a: Execute
        hymd_controller.execute_hymd_commands()

        # 2.b: Import MDs
        imp = HyMDImporter()
        imp.import_hymd_to_mdedup_discover()

        # 2.c: Calculate similarities for all available column matches
        sc = HyMDSimilaritiesCalculator()
        sc.calculate_deduplication_similarities()

        # Step 3: Discover and expand Matching Dependency Combinations
        dgs = MDCSelectionExpansion()
        dgs.execute_selection_and_expansion()

        # Step 4: Generate features for the discovered MDCs
        xplr = MDCExploration()
        xplr.execute_exploration()

        # Step 5: Predict MDCs
        prd = MDCPrediction()
        prd.execute_prediction()

        # Step 6: Apply boosting
        bst = MDCBoosting()
        bst.create_table_boosting()
        bst.execute_boosting()


if __name__ == '__main__':
    mdedup_controller = MDedupController()
    mdedup_controller.execute()
